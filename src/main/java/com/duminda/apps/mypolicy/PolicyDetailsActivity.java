
package com.duminda.apps.mypolicy;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.duminda.apps.common.DatePickerFragment;
import com.duminda.apps.common.JSONParser;
import com.duminda.apps.insuranceapp.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;
import java.util.Date;

// TODO: Auto-generated Javadoc

/**
 * The Class PolicyDetailsActivity.
 */
public class PolicyDetailsActivity extends Activity implements View.OnClickListener, DatePickerFragment.DatePickerFragmentListener {

    /**
     * The Cus_ id.
     */
    private EditText cusID;

    /**
     * The Policy_ id.
     */
    private EditText policyID;

    /**
     * The Exp_ date.
     */
    private EditText expDate;

    /**
     * The Rem_ days.
     */
    private EditText remainingDays;

    /**
     * The Ext_ given.
     */
    private EditText extGiven;

    /**
     * The Ext_ applied.
     */
    private EditText extApplied;

    /**
     * The App_ ext_ btn.
     */
    private Button appExtBtn;


    /**
     * The Cusid.
     */
    String CustomerId;
    /**
     * The days_rem.
     */
    private String daysRemaining;

    /**
     * The p dialog.
     */
    private ProgressDialog pDialog;

    /**
     * The Constant TAG_SUCCESS.
     */
    private static final String TAG_SUCCESS = "success";

    /**
     * The Constant TAG_POLID.
     */
    private static final String TAG_POLID = "PolicyID";

    /**
     * The Constant TAG_EXPDATE.
     */
    private static final String TAG_EXPDATE = "Exp_Date";

    /**
     * The Constant TAG_EXTAPP.
     */
    private static final String TAG_EXTAPP = "Extension_Applied";

    /**
     * The json parser.
     */
    JSONParser jsonParser = new JSONParser();

    /**
     * The Constant update_policy_url.
     */
    private String update_policy_url = "";

    /*
     * (non-Javadoc)
     *
     * @see android.app.Activity#onCreate(android.os.Bundle)
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.my_policy_detailes);

        update_policy_url = getResources().getString(R.string.hostAddress) + "index_update.php";

        Bundle extras = getIntent().getExtras();
        String arrayPdata[] = extras.getStringArray("Policy_Data");

        cusID = (EditText) findViewById(R.id.editText1);
        cusID.setText(arrayPdata[0]);
        cusID.setEnabled(false);
        CustomerId = arrayPdata[0];

        policyID = (EditText) findViewById(R.id.editText2);
        policyID.setText(arrayPdata[1]);
        policyID.setEnabled(false);

        expDate = (EditText) findViewById(R.id.editText3);
        expDate.setText(arrayPdata[2]);
        expDate.setEnabled(false);

        remainingDays = (EditText) findViewById(R.id.editText4);
        remainingDays.setText(arrayPdata[3]);
        daysRemaining = arrayPdata[3];
        remainingDays.setEnabled(false);

        extGiven = (EditText) findViewById(R.id.editText5);
        extGiven.setText(arrayPdata[4]);
        extGiven.setEnabled(false);

        extApplied = (EditText) findViewById(R.id.editText6);
        extApplied.setText(arrayPdata[5]);
        extApplied.setEnabled(false);

        appExtBtn = (Button) findViewById(R.id.apply_ext_btn);

        appExtBtn.setOnClickListener(this);

    }

    /*
     * (non-Javadoc)
     *
     * @see android.view.View.OnTouchListener#onTouch(android.view.View,
     * android.view.MotionEvent)
     */
    public void onClick(View v) {

        //setExtendedDate();

        DatePickerFragment newFragment = DatePickerFragment.newInstance(this);
        newFragment.show(getFragmentManager(), "datePicker");

    }

    @Override
    public void onDateSet(Date newDate) {

        Calendar cal = Calendar.getInstance();
        cal.setTime(newDate);
        int year = cal.get(Calendar.YEAR);
        int month = cal.get(Calendar.MONTH);
        int day = cal.get(Calendar.DAY_OF_MONTH);

        expDate.setText(new StringBuilder().append(year).append("-")
                .append(month + 1).append("-").append(day));
        extApplied.setText("YES");

        new UpdateExtendedPolicyDetails().execute();

        final Calendar c2 = Calendar.getInstance();
        c2.set(year, month, day);
        c2.get(Calendar.DATE);

        final Calendar c1 = Calendar.getInstance();
        c1.get(Calendar.DATE);

        int rem = c2.get(Calendar.DATE) - c1.get(Calendar.DATE)
                + Integer.parseInt(daysRemaining);

        remainingDays.setText("" + rem);
    }

    /**
     * The Class UpdateExtendedPolicyDetails.
     */
    class UpdateExtendedPolicyDetails extends AsyncTask<String, String, String> {

        /*
         * (non-Javadoc)
         *
         * @see android.os.AsyncTask#onPreExecute()
         */
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(PolicyDetailsActivity.this);
            pDialog.setMessage("Saving product ...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }

        /*
         * (non-Javadoc)
         *
         * @see android.os.AsyncTask#doInBackground(Params[])
         */
        @Override
        protected String doInBackground(String... arg0) {

            String new_exp_date = expDate.getText().toString();
            String new_ext_applied = extApplied.getText().toString();
            String policy_id = policyID.getText().toString();

            Uri.Builder builder = new Uri.Builder();
            builder.appendQueryParameter(TAG_POLID, policy_id);
            builder.appendQueryParameter(TAG_EXPDATE, new_exp_date);
            builder.appendQueryParameter(TAG_EXTAPP, new_ext_applied);

            JSONObject json2 = jsonParser.makeHttpRequest(update_policy_url,
                    "POST", builder);
            Log.d("Update Product", json2.toString());
            Log.i("Patameters", "[" + builder + "]");

            try {
                int success = json2.getInt(TAG_SUCCESS);

                if (success == 1) {
                    // successfully updated
                    Intent i = getIntent();
                    // send result code 100 to notify about product update
                    setResult(100, i);
                    finish();
                } else {
                    // failed to update product
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }

        /*
         * (non-Javadoc)
         *
         * @see android.os.AsyncTask#onPostExecute(java.lang.Object)
         */
        protected void onPostExecute(String file_url) {
            pDialog.dismiss();
        }
    }
}