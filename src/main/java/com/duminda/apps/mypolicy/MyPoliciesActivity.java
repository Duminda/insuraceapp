package com.duminda.apps.mypolicy;

import android.app.AlertDialog;
import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Toast;

import com.duminda.apps.common.JSONParser;
import com.duminda.apps.insuranceapp.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

// TODO: Auto-generated Javadoc

/**
 * The Class MyPoliciesActivity.
 */
public class MyPoliciesActivity extends ListActivity implements
        OnItemClickListener {

    /**
     * The pdialog.
     */
    private ProgressDialog pdialog;

    /**
     * The jparser.
     */
    JSONParser jparser = new JSONParser();

    /**
     * The policylist.
     */
    private ArrayList<HashMap<String, String>> policylist;

    /**
     * The policy_records_url.
     */
    private String policy_records_url = "";

    /**
     * The Constant TAG_SUCCESS.
     */
    private static final String TAG_SUCCESS = "success";

    /**
     * The Constant TAG_POLICIES.
     */
    private static final String TAG_POLICIES = "policyRecords";

    /**
     * The Constant TAG_POL_ID.
     */
    private static final String TAG_POL_ID = "PolicyID";

    /**
     * The Constant TAG_CUS_ID.
     */
    private static final String TAG_CUS_ID = "CusID";

    /**
     * The Constant TAG_DAYS_REM.
     */
    private static final String TAG_DAYS_REM = "No_Days";

    /**
     * The Constant TAG_EXP_DATE.
     */
    private static final String TAG_EXP_DATE = "Exp_Date";

    /**
     * The Constant TAG_EXT_GIV.
     */
    private static final String TAG_EXT_GIV = "Extension_Given";

    /**
     * The Constant TAG_EXT_APP.
     */
    private static final String TAG_EXT_APP = "Extension_Applied";

    /**
     * The policies.
     */
    JSONArray policies = null;

    /**
     * The days_rem.
     */
    private String days_rem;

    /**
     * The pol_id.
     */
    private String pol_id;

    /**
     * The cus_id.
     */
    private String cus_id;

    /**
     * The exp_date.
     */
    private String exp_date;

    /**
     * The ext_giv.
     */
    private String ext_giv;

    /**
     * The ext_app.
     */
    private String ext_app;

    /**
     * The g cusid.
     */
    private String gCusid;

    /* (non-Javadoc)
     * @see android.app.Activity#onCreate(android.os.Bundle)
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_policies);

        policylist = new ArrayList<HashMap<String, String>>();
        policy_records_url = getResources().getString(R.string.hostAddress)+"index_getall.php";

        Bundle extras = getIntent().getExtras();
        gCusid = extras.getString("globalCusID");

        new LoadAllPolicies().execute();

        ListView lv = getListView();

        lv.setOnItemClickListener(this);

    }

    /* (non-Javadoc)
     * @see android.widget.AdapterView.OnItemClickListener#onItemClick(android.widget.AdapterView, android.view.View, int, long)
     */
    public void onItemClick(AdapterView<?> parent, View view, int position,
                            long id) {

        HashMap<String, String> arr = policylist.get(position);

        // showExtExpireDialog();
        if (Integer.parseInt(arr.get(TAG_DAYS_REM)) <= 5) {
            Toast.makeText(
                    getApplicationContext(),
                    "Your policy will expire in " + arr.get(TAG_DAYS_REM)
                            + " days. Apply for extension please",
                    Toast.LENGTH_LONG).show();
        }
        launchPolicyDetails(arr.get(TAG_CUS_ID), arr.get(TAG_POL_ID),
                arr.get(TAG_EXP_DATE), arr.get(TAG_DAYS_REM),
                arr.get(TAG_EXT_GIV), arr.get(TAG_EXT_APP));

    }

    /**
     * Show ext expire dialog.
     */
    void showExtExpireDialog() {

        AlertDialog.Builder helpBuilder = new AlertDialog.Builder(this);
        helpBuilder.setTitle("Policy Expiration");
        helpBuilder.setMessage("Your policy will expire in " + TAG_DAYS_REM
                + " days. Apply for 30 days extension please.");

        helpBuilder.setPositiveButton("Ok",
                new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dlg, int arg1) {
                        dlg.cancel();

                    }
                });

        helpBuilder.setNegativeButton("Cancel",
                new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int which) {
                        MyPoliciesActivity.this.finish();

                    }
                });

        AlertDialog helpDialog = helpBuilder.create();
        helpDialog.show();

    }

    /**
     * Launch policy details.
     *
     * @param cus_id   the cus_id
     * @param pol_id   the pol_id
     * @param exp_date the exp_date
     * @param days_rem the days_rem
     * @param ext_giv  the ext_giv
     * @param ext_app  the ext_app
     */
    public void launchPolicyDetails(String cus_id, String pol_id,
                                    String exp_date, String days_rem, String ext_giv, String ext_app) {

        String pdata[] = {cus_id, pol_id, exp_date, days_rem, ext_giv, ext_app};

        Intent policyDetailsIntent = new Intent(this,
                PolicyDetailsActivity.class);
        policyDetailsIntent.putExtra("Policy_Data", pdata);
        startActivity(policyDetailsIntent);
    }

    /**
     * The Class LoadAllPolicies.
     */
    class LoadAllPolicies extends AsyncTask<String, String, String> {

        /* (non-Javadoc)
         * @see android.os.AsyncTask#onPreExecute()
         */
        @Override
        protected void onPreExecute() {

            super.onPreExecute();

            pdialog = new ProgressDialog(MyPoliciesActivity.this);
            pdialog.setMessage("Loading Policies ....");
            pdialog.setIndeterminate(false);
            pdialog.setCancelable(false);
            pdialog.show();
        }

        @Override
        protected String doInBackground(String... arg0) {


            Uri.Builder builder = new Uri.Builder();
            builder.appendQueryParameter(TAG_CUS_ID, gCusid);

            try {

                JSONObject json = jparser.makeHttpRequest(
                        policy_records_url, "GET", builder);

                //Log.v("JSON policy data : ", json.toString());

                int success = json.getInt(TAG_SUCCESS);

                if (success == 1) {

                    policies = json.getJSONArray(TAG_POLICIES);

                    for (int i = 0; i < policies.length(); i++) {
                        JSONObject c = policies.getJSONObject(i);

                        pol_id = c.getString(TAG_POL_ID);
                        days_rem = c.getString(TAG_DAYS_REM);
                        exp_date = c.getString(TAG_EXP_DATE);
                        cus_id = c.getString(TAG_CUS_ID);
                        ext_giv = c.getString(TAG_EXT_GIV);
                        ext_app = c.getString(TAG_EXT_APP);

                        HashMap<String, String> map = new HashMap<String,String>();

                        map.put(TAG_CUS_ID, cus_id);
                        map.put(TAG_POL_ID, pol_id);
                        map.put(TAG_EXP_DATE, exp_date);
                        map.put(TAG_DAYS_REM, days_rem);
                        map.put(TAG_EXT_GIV, ext_giv);
                        map.put(TAG_EXT_APP, ext_app);

                        policylist.add(map);

                    }
                } else {

                }
            } catch (JSONException e) {
                e.printStackTrace();
            }


            return null;
        }

        /* (non-Javadoc)
         * @see android.os.AsyncTask#onPostExecute(java.lang.Object)
         */
        protected void onPostExecute(String file_url) {
            // dismiss the dialog after getting all products
            pdialog.dismiss();

            // updating UI from Background Thread
            runOnUiThread(new Runnable() {
                public void run() {
                    /**
                     * Updating parsed JSON data into ListView
                     * */
                    ListAdapter adapter = new SimpleAdapter(
                            MyPoliciesActivity.this, policylist,
                            R.layout.list_item, new String[]{TAG_POL_ID,
                            TAG_DAYS_REM, TAG_EXP_DATE}, new int[]{
                            R.id.policy_id_txtview,
                            R.id.days_rem_txtview,
                            R.id.exp_date_txtview});
                    // updating listview
                    setListAdapter(adapter);

                }

            });

        }

    }

}
