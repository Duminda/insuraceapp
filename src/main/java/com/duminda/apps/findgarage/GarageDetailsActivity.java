package com.duminda.apps.findgarage;

import android.app.ListActivity;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import com.duminda.apps.common.JSONParser;
import com.duminda.apps.insuranceapp.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

// TODO: Auto-generated Javadoc

/**
 * The Class GarageDetailsActivity.
 */
public class GarageDetailsActivity extends ListActivity implements
        OnClickListener, OnItemClickListener {

    /**
     * The gar name txt.
     */
    private EditText garNameTxt;

    /**
     * The gar address txt.
     */
    private EditText garAddressTxt;

    /**
     * The gar contct no txt.
     */
    private EditText garContctNoTxt;

    /**
     * The view reviews btn.
     */
    private Button viewReviewsBtn;

    /**
     * The add reviews btn.
     */
    private Button addReviewsBtn;

    /**
     * The gar ratingbar.
     */
    private RatingBar garRatingbar;

    /**
     * The gjparser.
     */
    JSONParser gjparser = new JSONParser();

    /**
     * The garagedata.
     */
    JSONArray garagedata = null;

    /**
     * The garagescheduledata.
     */
    JSONArray garagescheduledata = null;

    /**
     * The garage_records_url.
     */
    private String garage_records_url = "";

    /**
     * The garage_schedule_url.
     */
    private String garage_schedule_url = "";

    /**
     * The schedulelist.
     */
    private ArrayList<HashMap<String, String>> schedulelist;

    /**
     * The Constant TAG_SUCCESS.
     */
    private static final String TAG_SUCCESS = "success";

    /**
     * The Constant TAG_GARAGE.
     */
    private static final String TAG_GARAGE = "garageData";

    /**
     * The Constant TAG_GARAGE_SCHEDULE.
     */
    private static final String TAG_GARAGE_SCHEDULE = "garageSchedule";

    /**
     * The Constant TAG_GAR_ID.
     */
    private static final String TAG_GAR_ID = "GarageID";

    /**
     * The Constant TAG_GAR_NAME.
     */
    private static final String TAG_GAR_NAME = "Name";

    /**
     * The Constant TAG_GAR_ADDRESS.
     */
    private static final String TAG_GAR_ADDRESS = "Address";

    /**
     * The Constant TAG_GAR_CONTACTNO.
     */
    private static final String TAG_GAR_CONTACTNO = "Contact";

    /**
     * The Constant TAG_GAR_POINTS.
     */
    private static final String TAG_GAR_POINTS = "Points";

    /**
     * The Constant TAG_GAR_SHE_DATE.
     */
    private static final String TAG_GAR_SHE_DATE = "Date";

    /**
     * The Constant TAG_GAR_SHE_AV1.
     */
    private static final String TAG_GAR_SHE_AV1 = "Availability_TS1";

    /**
     * The Constant TAG_GAR_SHE_AV2.
     */
    private static final String TAG_GAR_SHE_AV2 = "Availability_TS2";

    /**
     * The Constant TAG_GAR_SHE_AV3.
     */
    private static final String TAG_GAR_SHE_AV3 = "Availability_TS3";

    /**
     * The TA g_ ga r_ sh e_ boo k1.
     */
    private String TAG_GAR_SHE_BOOK1 = "Book_TS1";

    /**
     * The TA g_ ga r_ sh e_ boo k2.
     */
    private String TAG_GAR_SHE_BOOK2 = "Book_TS2";

    /**
     * The TA g_ ga r_ sh e_ boo k3.
     */
    private String TAG_GAR_SHE_BOOK3 = "Book_TS3";

    /**
     * The gar id.
     */
    private String garID;

    /**
     * The gar nm.
     */
    private String garNm;

    /**
     * The gar addr.
     */
    private String garAddr;

    /**
     * The gar contct.
     */
    private String garContct;

    /**
     * The gar points.
     */
    private String garPoints;

    /**
     * The date.
     */
    private String date;

    /**
     * The av1.
     */
    private String av1;

    /**
     * The av2.
     */
    private String av2;

    /**
     * The av3.
     */
    private String av3;

    /**
     * The b1.
     */
    private String b1;

    /**
     * The b2.
     */
    private String b2;

    /**
     * The b3.
     */
    private String b3;

    /**
     * The Row as string1.
     */
    String RowAsString1;

    /**
     * The Row as string2.
     */
    String RowAsString2;

    /**
     * The Row as string3.
     */
    String RowAsString3;

    /**
     * The TS string1.
     */
    String TSString1;

    /**
     * The TS string2.
     */
    String TSString2;

    /**
     * The TS string3.
     */
    String TSString3;

    /* (non-Javadoc)
     * @see android.app.Activity#onCreate(android.os.Bundle)
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.garage_details);

        garage_records_url = getResources().getString(R.string.hostAddress)+"index_getgarage.php";
        garage_schedule_url = getResources().getString(R.string.hostAddress)+"index_getgarage_schedule.php";

        schedulelist = new ArrayList<HashMap<String, String>>();

        Intent intnt = getIntent();
        garNm = intnt.getStringExtra("GarageName");

        garNameTxt = (EditText) findViewById(R.id.garageName_editText);
        garAddressTxt = (EditText) findViewById(R.id.garage_address_editText);
        garContctNoTxt = (EditText) findViewById(R.id.garage_contact_no_editText);
        garRatingbar = (RatingBar) findViewById(R.id.garage_ratingBar);
        viewReviewsBtn = (Button) findViewById(R.id.reviewBtn);
        addReviewsBtn = (Button) findViewById(R.id.add_reviewBtn);

        retriveGarageData();

        garNameTxt.setText(garNm);
        garAddressTxt.setText(garAddr);
        garContctNoTxt.setText(garContct);
        float ratingValue = Float.parseFloat(garPoints);
        garRatingbar.setRating(ratingValue);

        new LoadGarageData().execute();

        viewReviewsBtn.setOnClickListener(this);
        addReviewsBtn.setOnClickListener(this);

        ListView lv = getListView();
        lv.setOnItemClickListener(this);

    }

    /* (non-Javadoc)
     * @see android.view.View.OnClickListener#onClick(android.view.View)
     */
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.reviewBtn:
                launchReviews();
                break;
            case R.id.add_reviewBtn:
                launchAddReviews();
                break;
            default:
                break;
        }

    }

    /* (non-Javadoc)
     * @see android.widget.AdapterView.OnItemClickListener#onItemClick(android.widget.AdapterView, android.view.View, int, long)
     */
    public void onItemClick(AdapterView<?> adptr, View v, int position, long id) {

        Adapter ad = adptr.getAdapter();
        View listViewRow = ad.getView(position, v, null);
        TextView RowView1 = (TextView) listViewRow
                .findViewById(R.id.load_garg_book1);
        TextView RowView2 = (TextView) listViewRow
                .findViewById(R.id.load_garg_book2);
        TextView RowView3 = (TextView) listViewRow
                .findViewById(R.id.load_garg_book3);

        TextView RowView4 = (TextView) listViewRow
                .findViewById(R.id.load_garg_shedule_timeslot1);
        TextView RowView5 = (TextView) listViewRow
                .findViewById(R.id.load_garg_shedule_timeslot2);
        TextView RowView6 = (TextView) listViewRow
                .findViewById(R.id.load_garg_shedule_timeslot3);

        RowAsString1 = RowView1.getText().toString();
        RowAsString2 = RowView2.getText().toString();
        RowAsString3 = RowView3.getText().toString();

        TSString1 = RowView4.getText().toString();
        TSString2 = RowView5.getText().toString();
        TSString3 = RowView6.getText().toString();

        RowView1.setOnClickListener(new OnClickListener() {

            public void onClick(View v) {

                if (RowAsString1.equals("Book")) {
                    loadAddReview(TSString1);
                }

            }
        });

        RowView2.setOnClickListener(new OnClickListener() {

            public void onClick(View v) {

                if (RowAsString2.equals("Book")) {
                    loadAddReview(TSString2);
                }

            }
        });

        RowView3.setOnClickListener(new OnClickListener() {

            public void onClick(View v) {

                if (RowAsString3.equals("Book")) {
                    loadAddReview(TSString3);
                }

            }
        });
    }

    /**
     * Load add review.
     *
     * @param ts the ts
     */
    private void loadAddReview(String ts) {
        Intent addAppoIntn = new Intent(this, AddAppointmentActivity.class);
        addAppoIntn.putExtra("GarageID", garID);
        addAppoIntn.putExtra("TimeSlot", ts);
        startActivity(addAppoIntn);
    }

    /* (non-Javadoc)
     * @see android.app.Activity#onCreateOptionsMenu(android.view.Menu)
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.garage_details, menu);
        return true;
    }

    /**
     * Launch add reviews.
     */
    private void launchAddReviews() {

        Intent loadAddReviewsInt = new Intent(getApplicationContext(),
                AddReviewActivity.class);
        loadAddReviewsInt.putExtra("GarageID", garID);
        startActivity(loadAddReviewsInt);
    }

    /**
     * Launch reviews.
     */
    private void launchReviews() {

        Intent loadReviewsInt = new Intent(getApplicationContext(),
                ReviewsActivity.class);
        loadReviewsInt.putExtra("GarageID", garID);
        startActivity(loadReviewsInt);
    }

    /**
     * The Class LoadGarageData.
     */
    class LoadGarageData extends AsyncTask<String, String, String> {

        /* (non-Javadoc)
         * @see android.os.AsyncTask#doInBackground(Params[])
         */
        @Override
        protected String doInBackground(String... arg0) {

            Uri.Builder builder = new Uri.Builder();

            try {

                builder.appendQueryParameter(TAG_GAR_ID, garID);

                JSONObject json = gjparser.makeHttpRequest(garage_schedule_url,
                        "GET", builder);

                //Log.v("JSON garage schedule data : ", json.);

                // Checking for SUCCESS TAG
                int success = json.getInt(TAG_SUCCESS);

                if (success == 1) {

                    garagescheduledata = json.getJSONArray(TAG_GARAGE_SCHEDULE);

                    for (int i = 0; i < garagescheduledata.length(); i++) {
                        JSONObject c = garagescheduledata.getJSONObject(i);

                        // Storing each json item in variable

                        date = c.getString(TAG_GAR_SHE_DATE);
                        av1 = c.getString(TAG_GAR_SHE_AV1);
                        av2 = c.getString(TAG_GAR_SHE_AV2);
                        av3 = c.getString(TAG_GAR_SHE_AV3);

                        HashMap<String, String> map = new HashMap<String, String>();

                        // adding each child node to HashMap key =>
                        // value
                        map.put(TAG_GAR_SHE_DATE, date);
                        map.put(TAG_GAR_SHE_AV1, av1);
                        map.put(TAG_GAR_SHE_AV2, av2);
                        map.put(TAG_GAR_SHE_AV3, av3);

                        SpannableString sstring = new SpannableString("Book");
                        sstring.setSpan(new UnderlineSpan(), 0,
                                sstring.length(), 0);

                        if (av1.equals("Available")) {
                            b1 = "" + sstring;
                        } else {
                            b1 = "";
                        }
                        if (av2.equals("Available")) {
                            b2 = "" + sstring;
                        } else {
                            b2 = "";
                        }
                        if (av3.equals("Available")) {
                            b3 = "" + sstring;
                        } else {
                            b3 = "";
                        }

                        map.put(TAG_GAR_SHE_BOOK1, b1);
                        map.put(TAG_GAR_SHE_BOOK2, b2);
                        map.put(TAG_GAR_SHE_BOOK3, b3);

                        // adding HashList to ArrayList
                        schedulelist.add(map);

                    }

                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }

        /* (non-Javadoc)
         * @see android.os.AsyncTask#onPostExecute(java.lang.Object)
         */
        protected void onPostExecute(String file_url) {

            // updating UI from Background Thread
            runOnUiThread(new Runnable() {
                public void run() {

                    /**
                     * Updating parsed JSON data into ListView
                     * */
                    ListAdapter adapter = new SimpleAdapter(
                            GarageDetailsActivity.this, schedulelist,
                            R.layout.garage_schedule_list_item, new String[]{
                            TAG_GAR_SHE_DATE, TAG_GAR_SHE_AV1,
                            TAG_GAR_SHE_AV2, TAG_GAR_SHE_AV3,
                            TAG_GAR_SHE_BOOK1, TAG_GAR_SHE_BOOK2,
                            TAG_GAR_SHE_BOOK3}, new int[]{
                            R.id.load_garg_shedule_dt,
                            R.id.load_garg_time_availability1,
                            R.id.load_garg_time_availability2,
                            R.id.load_garg_time_availability3,
                            R.id.load_garg_book1, R.id.load_garg_book2,
                            R.id.load_garg_book3});
                    setListAdapter(adapter);
                    /*
					 * 
					 * View listViewRow = adapter.getView(0, null, null);
					 * 
					 * SpannableString sstring1=new
					 * SpannableString(TAG_GAR_SHE_BOOK1); sstring1.setSpan(new
					 * UnderlineSpan(), 0, sstring1.length(), 0); TextView
					 * tv=(TextView
					 * )listViewRow.findViewById(R.id.load_garg_book1);
					 * tv.setText(sstring1);
					 */

                }

            });

        }
    }

    /**
     * Retrive garage data.
     */
    private void retriveGarageData() {

        Uri.Builder builder = new Uri.Builder();

        try {

            builder.appendQueryParameter(TAG_GAR_NAME, garNm);

            JSONObject json = gjparser.makeHttpRequest(garage_records_url,
                    "GET", builder);

            ///Log.v("JSON garage data : ", json.toString());

            // Checking for SUCCESS TAG
            int success = json.getInt(TAG_SUCCESS);

            if (success == 1) {

                garagedata = json.getJSONArray(TAG_GARAGE);

                for (int i = 0; i < garagedata.length(); i++) {
                    JSONObject c = garagedata.getJSONObject(i);

                    // Storing each json item in variable
                    garID = c.getString(TAG_GAR_ID);
                    garNm = c.getString(TAG_GAR_NAME);
                    garAddr = c.getString(TAG_GAR_ADDRESS);
                    garContct = c.getString(TAG_GAR_CONTACTNO);
                    garPoints = c.getString(TAG_GAR_POINTS);

                }

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

}
