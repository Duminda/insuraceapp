package com.duminda.apps.findgarage;

import android.app.Activity;
import android.app.ProgressDialog;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Menu;

import com.duminda.apps.common.JSONParser;
import com.duminda.apps.insuranceapp.R;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.GroundOverlay;
import com.google.android.gms.maps.model.GroundOverlayOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;


// TODO: Auto-generated Javadoc

/**
 * The Class FindGarageActivity.
 */
public class FindGarageActivity extends Activity {

    /**
     * The garages overlay.
     */

    private List<GroundOverlay> garagesOverlay = new ArrayList<GroundOverlay>();

    /**
     * The garage_records_url.
     */
    private String garage_records_url = "";

    /**
     * The garages.
     */
    JSONArray garages = null;

    /**
     * The jparser.
     */
    JSONParser jparser = new JSONParser();

    /**
     * The gp.
     */
    //private GeoPoint[] gp = new GeoPoint[10];
    private LatLng[] gp = new LatLng[10];

    /**
     * The tp.
     */
    private String[] tp = new String[10];

    /**
     * The garage.
     */
    private String[] garage = new String[10];

    /**
     * The Constant TAG_SUCCESS.
     */
    private static final String TAG_SUCCESS = "success";

    /**
     * The Constant TAG_GARAGES.
     */
    private static final String TAG_GARAGES = "Garages";

    /**
     * The Constant TAG_GARAGE_NAME.
     */
    private static final String TAG_GARAGE_NAME = "Name";

    /**
     * The Constant TAG_GARAGE_TP.
     */
    private static final String TAG_GARAGE_TP = "Contact";

    /**
     * The Constant TAG_GARAGE_LAT.
     */
    private static final String TAG_GARAGE_LAT = "Latitude";

    /**
     * The Constant TAG_GARAGE_LON.
     */
    private static final String TAG_GARAGE_LON = "Longitude";

    /**
     * The garage_name.
     */
    private String garage_name;

    /**
     * The garage_tp.
     */
    private String garage_tp;

    /**
     * The garage_lat.
     */
    private String garage_lat;

    /**
     * The garage_lon.
     */
    private String garage_lon;

    /**
     * The mviewgarage.
     */
    private MapView mviewgarage;

    /**
     * The pdialog.
     */
    private ProgressDialog pdialog;

    private GoogleMap map;

    private GroundOverlayOptions makerGarage;

    /* (non-Javadoc)
     * @see com.google.android.maps.MapActivity#onCreate(android.os.Bundle)
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.find_garage);

        garage_records_url = getResources().getString(R.string.hostAddress)+"index_getgarage_locations.php";

        map = ((MapFragment) getFragmentManager().findFragmentById(R.id.findGaragemapView))
                .getMap();

        new LoadGaragesData().execute();

    }

    /* (non-Javadoc)
     * @see android.app.Activity#onCreateOptionsMenu(android.view.Menu)
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.find_garage, menu);
        return true;
    }


    /**
     * The Class LoadGaragesData.
     */
    class LoadGaragesData extends AsyncTask<String, String, String> {

        /* (non-Javadoc)
         * @see android.os.AsyncTask#onPreExecute()
         */
        @Override
        protected void onPreExecute() {

            super.onPreExecute();

            pdialog = new ProgressDialog(FindGarageActivity.this);
            pdialog.setMessage("Loading Garages ....");
            pdialog.setIndeterminate(false);
            pdialog.setCancelable(false);
            pdialog.show();
        }

        /* (non-Javadoc)
         * @see android.os.AsyncTask#doInBackground(Params[])
         */
        @Override
        protected String doInBackground(String... arg0) {
            try {

                Uri.Builder builder = new Uri.Builder();

                JSONObject json = jparser.makeHttpRequest(garage_records_url,
                        "GET", builder);

                //Log.v("JSON agent data : ", json.toString());

                // Checking for SUCCESS TAG
                int success = json.getInt(TAG_SUCCESS);

                if (success == 1) {

                    garages = json.getJSONArray(TAG_GARAGES);


                } else {

                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        /* (non-Javadoc)
         * @see android.os.AsyncTask#onPostExecute(java.lang.Object)
         */
        protected void onPostExecute(String file_url) {
            // dismiss the dialog after getting all products

            for (int i = 0; i < garages.length(); i++) {

                try {
                    JSONObject c = garages.getJSONObject(i);

                    // Storing each json item in variable
                    garage_name = c.getString(TAG_GARAGE_NAME);
                    garage_tp = c.getString(TAG_GARAGE_TP);
                    garage_lat = c.getString(TAG_GARAGE_LAT);
                    garage_lon = c.getString(TAG_GARAGE_LON);

                    double lat = Double.parseDouble(garage_lat);
                    double lon = Double.parseDouble(garage_lon);


                    gp[i] = new LatLng(lat, lon);
                    tp[i] = garage_tp;
                    garage[i] = garage_name;

                    if (map != null) {
                        Marker agent = map.addMarker(new MarkerOptions()
                                .position(gp[i])
                                .icon(BitmapDescriptorFactory.fromResource(R.drawable.garage_marker)));
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            map.moveCamera(CameraUpdateFactory.newLatLngZoom(gp[0], 15));

            pdialog.dismiss();
        }

    }
}
