package com.duminda.apps.findgarage;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.widget.RatingBar;
import android.widget.TextView;

import com.duminda.apps.insuranceapp.R;

// TODO: Auto-generated Javadoc

/**
 * The Class ReviewDetailsActivity.
 */
public class ReviewDetailsActivity extends Activity {

    /**
     * The title.
     */
    private TextView title;

    /**
     * The comment.
     */
    private TextView comment;

    /**
     * The date.
     */
    private TextView date;

    /**
     * The name.
     */
    private TextView name;

    /**
     * The customer rating.
     */
    private RatingBar customerRating;

    /* (non-Javadoc)
     * @see android.app.Activity#onCreate(android.os.Bundle)
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.review_details);

        Intent rint = getIntent();
        String rArray[] = rint.getStringArrayExtra("ReviewDetails");

        title = (TextView) findViewById(R.id.title_textView);
        comment = (TextView) findViewById(R.id.comment_textView);
        name = (TextView) findViewById(R.id.name_textView);
        date = (TextView) findViewById(R.id.date_textView);
        customerRating = (RatingBar) findViewById(R.id.customer_ratingBar);

        title.setText(rArray[0]);
        comment.setText(rArray[1]);
        name.setText(rArray[3]);
        date.setText(rArray[4]);

        float ratingValue = Float.parseFloat(rArray[2]);
        customerRating.setRating(ratingValue);

    }

    /* (non-Javadoc)
     * @see android.app.Activity#onCreateOptionsMenu(android.view.Menu)
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.review_details, menu);
        return true;
    }
}
