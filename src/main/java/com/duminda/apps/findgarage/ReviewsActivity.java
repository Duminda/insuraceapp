package com.duminda.apps.findgarage;

import android.app.ListActivity;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.SimpleAdapter;

import com.duminda.apps.common.JSONParser;
import com.duminda.apps.insuranceapp.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

;

// TODO: Auto-generated Javadoc

/**
 * The Class ReviewsActivity.
 */
public class ReviewsActivity extends ListActivity implements
        OnItemClickListener {

    /**
     * The garage id.
     */
    private String garageID;

    /**
     * The garage nm.
     */
    private String garageNm;

    /**
     * The garage date.
     */
    private String garageDate;

    /**
     * The garage title.
     */
    private String garageTitle;

    /**
     * The garage comment.
     */
    private String garageComment;

    /**
     * The garage rating.
     */
    private String garageRating;

    /**
     * The Constant TAG_SUCCESS.
     */
    private static final String TAG_SUCCESS = "success";

    /**
     * The Constant TAG_REVIEWS.
     */
    private static final String TAG_REVIEWS = "garageReviews";

    /**
     * The Constant TAG_GARAGE_ID.
     */
    private static final String TAG_GARAGE_ID = "GarageID";

    /**
     * The Constant TAG_REVIEW_NAME.
     */
    private static final String TAG_REVIEW_NAME = "Name";

    /**
     * The Constant TAG_REVIEW_DATE.
     */
    private static final String TAG_REVIEW_DATE = "Date";

    /**
     * The Constant TAG_REVIEW_TITLE.
     */
    private static final String TAG_REVIEW_TITLE = "Title";

    /**
     * The Constant TAG_REVIEW_COMMENT.
     */
    private static final String TAG_REVIEW_COMMENT = "Comment";

    /**
     * The Constant TAG_REVIEW_RATING.
     */
    private static final String TAG_REVIEW_RATING = "Rating";

    /**
     * The garage_review_url.
     */
    private String garage_review_url = "";

    /**
     * The reviewlist.
     */
    private ArrayList<HashMap<String, String>> reviewlist;

    /**
     * The grjparser.
     */
    JSONParser grjparser = new JSONParser();

    /**
     * The garagereviews.
     */
    JSONArray garagereviews = null;

    /* (non-Javadoc)
     * @see android.app.Activity#onCreate(android.os.Bundle)
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.reviews);

        garage_review_url = getResources().getString(R.string.hostAddress)+"index_get_reviews.php";

        Intent intnt = getIntent();
        garageID = intnt.getStringExtra("GarageID");

        reviewlist = new ArrayList<HashMap<String, String>>();

        new LoadReviewData().execute();

        ListView reviewList = getListView();

        reviewList.setOnItemClickListener(this);
    }

    /* (non-Javadoc)
     * @see android.widget.AdapterView.OnItemClickListener#onItemClick(android.widget.AdapterView, android.view.View, int, long)
     */
    public void onItemClick(AdapterView<?> arg0, View arg1, int position,
                            long id) {

        HashMap<String, String> arr = reviewlist.get(position);

        String reviewArray[] = {arr.get(TAG_REVIEW_TITLE),
                arr.get(TAG_REVIEW_COMMENT), arr.get(TAG_REVIEW_RATING),
                arr.get(TAG_REVIEW_NAME), arr.get(TAG_REVIEW_DATE)};

        launchReviewDetails(reviewArray);
    }

    /**
     * Launch review details.
     *
     * @param rArray the r array
     */
    private void launchReviewDetails(String[] rArray) {

        Intent reviewDetailsIntn = new Intent(this, ReviewDetailsActivity.class);
        reviewDetailsIntn.putExtra("ReviewDetails", rArray);
        startActivity(reviewDetailsIntn);
    }

    /* (non-Javadoc)
     * @see android.app.Activity#onCreateOptionsMenu(android.view.Menu)
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.reviews, menu);
        return true;
    }

    /**
     * The Class LoadReviewData.
     */
    class LoadReviewData extends AsyncTask<String, String, String> {

        /* (non-Javadoc)
         * @see android.os.AsyncTask#doInBackground(Params[])
         */
        @Override
        protected String doInBackground(String... arg0) {

            Uri.Builder builder = new Uri.Builder();

            try {

                builder.appendQueryParameter(TAG_GARAGE_ID, garageID);

                JSONObject json = grjparser.makeHttpRequest(garage_review_url,
                        "GET", builder);

                //Log.v("JSON garage review data : ", json.toString());

                int success = json.getInt(TAG_SUCCESS);

                if (success == 1) {

                    garagereviews = json.getJSONArray(TAG_REVIEWS);

                    for (int i = 0; i < garagereviews.length(); i++) {
                        JSONObject c = garagereviews.getJSONObject(i);

                        garageTitle = c.getString(TAG_REVIEW_TITLE);
                        garageComment = c.getString(TAG_REVIEW_COMMENT);
                        garageRating = c.getString(TAG_REVIEW_RATING);
                        garageNm = c.getString(TAG_REVIEW_NAME);
                        garageDate = c.getString(TAG_REVIEW_DATE);

                        HashMap<String, String> map = new HashMap<String, String>();

                        map.put(TAG_REVIEW_TITLE, garageTitle);
                        map.put(TAG_REVIEW_COMMENT, garageComment);
                        map.put(TAG_REVIEW_RATING, garageRating);
                        map.put(TAG_REVIEW_NAME, garageNm);
                        map.put(TAG_REVIEW_DATE, garageDate);

                        reviewlist.add(map);

                    }

                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }

        /* (non-Javadoc)
         * @see android.os.AsyncTask#onPostExecute(java.lang.Object)
         */
        protected void onPostExecute(String file_url) {

            runOnUiThread(new Runnable() {
                public void run() {

                    // RatingBar customerRating=(RatingBar)
                    // findViewById(R.id.customer_ratingBar);
                    // int ratingval=Integer.parseInt(TAG_REVIEW_RATING);
                    // customerRating.setRating(4);
                    /**
                     * Updating parsed JSON data into ListView
                     * */
                    ListAdapter adapter = new SimpleAdapter(
                            ReviewsActivity.this, reviewlist,
                            R.layout.reviews_list_item, new String[]{
                            TAG_REVIEW_TITLE, TAG_REVIEW_COMMENT,
                            TAG_REVIEW_RATING, TAG_REVIEW_NAME,
                            TAG_REVIEW_DATE}, new int[]{
                            R.id.title_textView, R.id.comment_textView,
                            R.id.customer_ratingBar,
                            R.id.name_textView, R.id.date_textView});

                    ((SimpleAdapter) adapter).setViewBinder(new MyBinder());

                    setListAdapter(adapter);

                }

            });

        }
    }

    /**
     * The Class MyBinder.
     */
    class MyBinder implements android.widget.SimpleAdapter.ViewBinder {

        /* (non-Javadoc)
         * @see android.widget.SimpleAdapter.ViewBinder#setViewValue(android.view.View, java.lang.Object, java.lang.String)
         */
        public boolean setViewValue(View view, Object data,
                                    String textRepresentation) {

            if (view.getId() == R.id.customer_ratingBar) {

                String stringval = (String) data;

                float ratingValue = Float.parseFloat(stringval);

                RatingBar ratingBar = (RatingBar) view;

                ratingBar.setRating(ratingValue);

                return true;

            }

            return false;

        }

        /**
         * Sets the view value.
         *
         * @param arg0 the arg0
         * @param arg1 the arg1
         * @param arg2 the arg2
         * @return true, if successful
         */
        public boolean setViewValue(View arg0, Cursor arg1, int arg2) {

            // TODO Auto-generated method stub

            return false;

        }

    }

}
