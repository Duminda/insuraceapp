package com.duminda.apps.findgarage;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.duminda.apps.common.JSONParser;
import com.duminda.apps.insuranceapp.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;

;

// TODO: Auto-generated Javadoc

/**
 * The Class AddAppointmentActivity.
 */
public class AddAppointmentActivity extends Activity implements View.OnClickListener {

    /**
     * The appo date.
     */
    private String appoDate;

    /**
     * The appo time slot.
     */
    private String appoTimeSlot;

    /**
     * The appo name.
     */
    private String appoName;

    /**
     * The appo addr.
     */
    private String appoAddr;

    /**
     * The appo contct no.
     */
    private String appoContctNo;

    /**
     * The appo descr.
     */
    private String appoDescr;

    /**
     * The garage id.
     */
    private String garageID;

    /**
     * The time slot.
     */
    private String timeSlot;

    /**
     * The adate.
     */
    private EditText adate;

    /**
     * The atimeslot.
     */
    private EditText atimeslot;

    /**
     * The aname.
     */
    private EditText aname;

    /**
     * The aaddr.
     */
    private EditText aaddr;

    /**
     * The acno.
     */
    private EditText acno;

    /**
     * The adescr.
     */
    private EditText adescr;

    /**
     * The p dialog.
     */
    private ProgressDialog pDialog;

    /**
     * The save appo.
     */
    private Button saveAppo;

    /**
     * The Constant TAG_SUCCESS.
     */
    private static final String TAG_SUCCESS = "success";

    /**
     * The Constant TAG_GARID.
     */
    private static final String TAG_GARID = "GarageID";

    /**
     * The Constant TAG_TIMESLOT.
     */
    private static final String TAG_TIMESLOT = "TimeSlot";

    /**
     * The Constant TAG_NAME.
     */
    private static final String TAG_NAME = "Name";

    /**
     * The Constant TAG_ADDR.
     */
    private static final String TAG_ADDR = "Address";

    /**
     * The Constant TAG_CONTNO.
     */
    private static final String TAG_CONTNO = "ContactNo";

    /**
     * The Constant TAG_DESCR.
     */
    private static final String TAG_DESCR = "Description";

    /**
     * The Constant TAG_DATE.
     */
    private static final String TAG_DATE = "Date";

    /**
     * The json parser.
     */
    JSONParser jsonParser = new JSONParser();

    /**
     * The Constant add_appointment_url.
     */
    private String add_appointment_url = "";

    /* (non-Javadoc)
     * @see android.app.Activity#onCreate(android.os.Bundle)
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_appointment);

        add_appointment_url = getResources().getString(R.string.hostAddress)+"index_appointment.php";

        Intent intn = getIntent();
        garageID = intn.getStringExtra("GarageID");
        timeSlot = intn.getStringExtra("TimeSlot");

        adate = (EditText) findViewById(R.id.add_appo_date_EditText);
        atimeslot = (EditText) findViewById(R.id.add_appo_time_EditText);
        aname = (EditText) findViewById(R.id.add_appo_name_EditText);
        aaddr = (EditText) findViewById(R.id.add_appo_addr_EditText);
        acno = (EditText) findViewById(R.id.add_appo_contno_EditText);
        adescr = (EditText) findViewById(R.id.add_appo_descr_EditText);
        saveAppo = (Button) findViewById(R.id.add_appo_ivw);

        adate.setText(getCurrentDate());
        atimeslot.setText(timeSlot);

        adate.setEnabled(false);
        atimeslot.setEnabled(false);

        saveAppo.setOnClickListener(this);

    }

    /* (non-Javadoc)
     * @see android.view.View.OnTouchListener#onTouch(android.view.View, android.view.MotionEvent)
     */
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.add_appo_ivw: {
                new SaveAppointment().execute();
                break;
            }
            default:
                break;
        }
    }

    /**
     * Gets the current date.
     *
     * @return the current date
     */
    private String getCurrentDate() {

        final Calendar c = Calendar.getInstance();

        int y = c.get(Calendar.YEAR);
        int m = c.get(Calendar.MONTH);
        int d = c.get(Calendar.DAY_OF_MONTH);

        return "" + y + "-" + m + "-" + d;

    }

    /* (non-Javadoc)
     * @see android.app.Activity#onCreateOptionsMenu(android.view.Menu)
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.add_appointment, menu);
        return true;
    }

    /**
     * The Class SaveAppointment.
     */
    class SaveAppointment extends AsyncTask<String, String, String> {

        /* (non-Javadoc)
         * @see android.os.AsyncTask#onPreExecute()
         */
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(AddAppointmentActivity.this);
            pDialog.setMessage("Adding appointment ...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }

        /* (non-Javadoc)
         * @see android.os.AsyncTask#doInBackground(Params[])
         */
        @Override
        protected String doInBackground(String... arg0) {

            appoDate = adate.getText().toString();
            appoTimeSlot = atimeslot.getText().toString();
            appoName = aname.getText().toString();
            appoAddr = aaddr.getText().toString();
            appoContctNo = acno.getText().toString();
            appoDescr = adescr.getText().toString();

            Uri.Builder builder = new Uri.Builder();

            builder.appendQueryParameter(TAG_GARID, garageID);
            builder.appendQueryParameter(TAG_DATE, appoDate);
            builder.appendQueryParameter(TAG_TIMESLOT, appoTimeSlot);
            builder.appendQueryParameter(TAG_NAME, appoName);
            builder.appendQueryParameter(TAG_ADDR, appoAddr);
            builder.appendQueryParameter(TAG_CONTNO, appoContctNo);
            builder.appendQueryParameter(TAG_DESCR, appoDescr);

            JSONObject json2 = jsonParser.makeHttpRequest(add_appointment_url,
                    "POST", builder);
            Log.d("Add Appointment", json2.toString());
            Log.i("Patameters", "[" + builder + "]");

            try {
                int success = json2.getInt(TAG_SUCCESS);

                if (success == 1) {
                    // successfully updated
                    Intent i = getIntent();
                    // send result code 100 to notify about product update
                    setResult(100, i);
                    finish();
                } else {
                    // failed to update product
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }

        /* (non-Javadoc)
         * @see android.os.AsyncTask#onPostExecute(java.lang.Object)
         */
        protected void onPostExecute(String file_url) {

            pDialog.dismiss();
        }

    }

}
