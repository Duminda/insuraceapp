package com.duminda.apps.findgarage;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.duminda.apps.common.JSONParser;
import com.duminda.apps.insuranceapp.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;

// TODO: Auto-generated Javadoc

/**
 * The Class AddReviewActivity.
 */
public class AddReviewActivity extends Activity implements View.OnClickListener {

    /**
     * The p dialog.
     */
    private ProgressDialog pDialog;

    /**
     * The garage id.
     */
    private String garageID;

    /**
     * The reviewer name.
     */
    private String reviewerName;

    /**
     * The reviewer date.
     */
    private String reviewerDate;

    /**
     * The reviewer title.
     */
    private String reviewerTitle;

    /**
     * The reviewer comment.
     */
    private String reviewerComment;

    /**
     * The reviewer rating.
     */
    private String reviewerRating;

    /**
     * The reviewer rating value.
     */
    private String reviewerRatingValue;

    /**
     * The c name.
     */
    private EditText cName;

    /**
     * The c date.
     */
    private TextView cDate;

    /**
     * The c title.
     */
    private EditText cTitle;

    /**
     * The c comment.
     */
    private EditText cComment;

    /**
     * The c rating.
     */
    private Spinner cRating;

    /**
     * The c save.
     */
    private Button cSave;

    /**
     * The Constant TAG_SUCCESS.
     */
    private static final String TAG_SUCCESS = "success";

    /**
     * The Constant TAG_GARAGE_ID.
     */
    private static final String TAG_GARAGE_ID = "GarageID";

    /**
     * The Constant TAG_NAME.
     */
    private static final String TAG_NAME = "Name";

    /**
     * The Constant TAG_DATE.
     */
    private static final String TAG_DATE = "Date";

    /**
     * The Constant TAG_TITLE.
     */
    private static final String TAG_TITLE = "Title";

    /**
     * The Constant TAG_COMMENTS.
     */
    private static final String TAG_COMMENTS = "Comment";

    /**
     * The Constant TAG_RATING.
     */
    private static final String TAG_RATING = "Rating";

    /**
     * The json parser.
     */
    JSONParser jsonParser = new JSONParser();

    /**
     * The ratinglist.
     */
    private ArrayList<String> ratinglist;

    /**
     * The Constant add_reviews_url.
     */
    private String add_reviews_url = "";

    /* (non-Javadoc)
     * @see android.app.Activity#onCreate(android.os.Bundle)
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_review);

        add_reviews_url = getResources().getString(R.string.hostAddress)+"index_add_review.php";

        Intent intnt = getIntent();
        garageID = intnt.getStringExtra("GarageID");

        cName = (EditText) findViewById(R.id.add_rvw_name_EditText);
        cDate = (TextView) findViewById(R.id.add_rvw_date_TextView);
        cComment = (EditText) findViewById(R.id.add_rvw_commnt_EditText);
        cTitle = (EditText) findViewById(R.id.add_rvw_title_EditText);
        cRating = (Spinner) findViewById(R.id.add_rvw_rating_Spinner);
        cSave = (Button) findViewById(R.id.save_review_ivw);

        cDate.setText(getCurrentDate());

        ratinglist = new ArrayList<String>();

        ratinglist.add("Satisfied");
        ratinglist.add("Good");
        ratinglist.add("Medium");
        ratinglist.add("Not Satisfied");

        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(
                AddReviewActivity.this,
                android.R.layout.simple_spinner_dropdown_item, ratinglist);
        cRating.setAdapter(spinnerArrayAdapter);

        cSave.setOnClickListener(this);
    }

    /* (non-Javadoc)
     * @see android.view.View.OnTouchListener#onTouch(android.view.View, android.view.MotionEvent)
     */
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.save_review_ivw:
                new AddGarageReview().execute();
                break;
        }
    }

    /**
     * Gets the current date.
     *
     * @return the current date
     */
    private String getCurrentDate() {

        final Calendar c = Calendar.getInstance();

        int y = c.get(Calendar.YEAR);
        int m = c.get(Calendar.MONTH);
        int d = c.get(Calendar.DAY_OF_MONTH);
        int h = c.get(Calendar.HOUR);
        int min = c.get(Calendar.MINUTE);

        return "" + y + "-" + m + "-" + d + " " + h + ":" + min;

    }

    /* (non-Javadoc)
     * @see android.app.Activity#onCreateOptionsMenu(android.view.Menu)
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.add_review, menu);
        return true;
    }

    /**
     * The Class AddGarageReview.
     */
    class AddGarageReview extends AsyncTask<String, String, String> {

        /* (non-Javadoc)
         * @see android.os.AsyncTask#onPreExecute()
         */
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(AddReviewActivity.this);
            pDialog.setMessage("Saving review ...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }

        /* (non-Javadoc)
         * @see android.os.AsyncTask#doInBackground(Params[])
         */
        @Override
        protected String doInBackground(String... arg0) {

            reviewerName = cName.getText().toString();
            reviewerDate = cDate.getText().toString();
            reviewerTitle = cTitle.getText().toString();
            reviewerComment = cComment.getText().toString();

            reviewerRating = cRating.getSelectedItem().toString();

            if (reviewerRating.equals("Satisfied")) {
                reviewerRatingValue = "5";
            }
            if (reviewerRating.equals("Good")) {
                reviewerRatingValue = "4";
            }
            if (reviewerRating.equals("Medium")) {
                reviewerRatingValue = "2";
            }
            if (reviewerRating.equals("Not Satisfied")) {
                reviewerRatingValue = "0";
            }


            Uri.Builder builder = new Uri.Builder();

            builder.appendQueryParameter(TAG_GARAGE_ID, garageID);
            builder.appendQueryParameter(TAG_NAME, reviewerName);
            builder.appendQueryParameter(TAG_DATE, reviewerDate);
            builder.appendQueryParameter(TAG_TITLE, reviewerTitle);
            builder.appendQueryParameter(TAG_COMMENTS, reviewerComment);
            builder.appendQueryParameter(TAG_RATING, reviewerRatingValue);


            JSONObject json2 = jsonParser.makeHttpRequest(add_reviews_url,
                    "POST", builder);
            Log.d("Add Review ", json2.toString());
            Log.i("Patameters", "[" + builder + "]");

            try {
                int success = json2.getInt(TAG_SUCCESS);

                if (success == 1) {
                    // successfully updated
                    Intent i = getIntent();
                    // send result code 100 to notify about product update
                    setResult(100, i);
                    finish();
                } else {
                    // failed to update product
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }

        /* (non-Javadoc)
         * @see android.os.AsyncTask#onPostExecute(java.lang.Object)
         */
        protected void onPostExecute(String file_url) {

            pDialog.dismiss();
        }

    }

}
