package com.duminda.apps.common;

import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Iterator;

/**
 * Created by duminda on 7/28/15.
 */
public class RestConnection {

    private JSONObject jObj = null;
    private String json = "";

    public JSONObject makeRestCall(String url, String method,
                                   HashMap<String ,String> map) {

        try {
            URL restUrl = new URL(url);
            HttpURLConnection conn = (HttpURLConnection) restUrl.openConnection();

            if(method.equals("GET")) {

                conn.setRequestMethod("GET");
                conn.setRequestProperty("Accept", "application/json");

                if (conn.getResponseCode() != 200) {
                    throw new RuntimeException("Failed : HTTP error code : "
                            + conn.getResponseCode());
                }

                BufferedReader br = new BufferedReader(new InputStreamReader(
                        (conn.getInputStream())));

                String output;
                StringBuffer response = new StringBuffer();
                while ((output = br.readLine()) != null) {
                    response.append(output);
                }

                json =  response.toString();
            }
            if (method.equals("POST")){

                conn.setDoOutput(true);
                conn.setRequestMethod("POST");
                conn.setRequestProperty("Content-Type", "application/json");

                String input = "{";

                Iterator it = map.entrySet().iterator();
                while (it.hasNext()) {
                    HashMap.Entry pair = (HashMap.Entry)it.next();

                    input +=  pair.getKey() + " : " + pair.getValue();
                    it.remove(); // avoids a ConcurrentModificationException
                    if(it.hasNext()) input += ",";
                }

                input += "}";

                //String input = "{\"qty\":100,\"name\":\"iPad 4\"}";

                OutputStream os = conn.getOutputStream();
                os.write(input.getBytes());
                os.flush();

                if (conn.getResponseCode() != HttpURLConnection.HTTP_CREATED) {
                    throw new RuntimeException("Failed : HTTP error code : "
                            + conn.getResponseCode());
                }

                BufferedReader br = new BufferedReader(new InputStreamReader(
                        (conn.getInputStream())));

                String output;
                StringBuffer response = new StringBuffer();
                while ((output = br.readLine()) != null) {
                    response.append(output);
                }

                json =  response.toString();

            }

            conn.disconnect();

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            jObj = new JSONObject(json);
        } catch (JSONException e) {
            Log.e("JSON Parser", "Error parsing data " + e.toString());
        }

        // return JSON String
        return jObj;
    }
}
