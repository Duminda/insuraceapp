package com.duminda.apps.common;

/**
 * Created by duminda on 5/26/15.
 */


import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.duminda.apps.createincidents.IncidentsData;

import java.util.ArrayList;
import java.util.List;

// TODO: Auto-generated Javadoc

/**
 * The Class IAppDBHelper.
 */
public class DBHelper extends SQLiteOpenHelper {

    /**
     * The Constant DB.
     */
    private static final String DB = "incident_db";

    /**
     * The Constant TABLE_CREATE_INC.
     */
    private static final String TABLE_CREATE_INC = "incident_tbl";

    /**
     * The Constant ID.
     */
    private static final String ID = "id";

    /**
     * The Constant CUSID.
     */
    private static final String CUSID = "Cus_ID";

    /**
     * The Constant VEH.
     */
    private static final String VEH = "Veh_NO";

    /**
     * The Constant MCLAUSE.
     */
    private static final String MCLAUSE = "Main_Cause";

    /**
     * The Constant SCLAUSE.
     */
    private static final String SCLAUSE = "Sub_Cause";

    /**
     * The Constant OWN.
     */
    private static final String OWN = "Owner";

    /**
     * The Constant NAME.
     */
    private static final String NAME = "Name";

    /**
     * The Constant LICENCENO.
     */
    private static final String LICENCENO = "Licence_NO";

    /**
     * The Constant DATE.
     */
    private static final String DATE = "Date";

    /**
     * The Constant LOC.
     */
    private static final String LOC = "Location";

    /**
     * The Constant SPD.
     */
    private static final String SPD = "Speed";

    /**
     * The Constant DESCR.
     */
    private static final String DESCR = "Description";

    /**
     * The Constant LAT.
     */
    private static final String LAT = "Latitude";

    /**
     * The Constant LON.
     */
    private static final String LON = "Longitude";

    /**
     * The Constant VERSION.
     */
    private static final int VERSION = 1;

    /**
     * The create db.
     */
    private static String CREATE_DB = "CREATE TABLE " + TABLE_CREATE_INC + "("
            + ID + " integer primary key autoincrement," + CUSID
            + " text not null," + VEH + " text not null," + MCLAUSE
            + " text not null," + SCLAUSE + " text not null," + OWN
            + " text not null," + NAME + " text not null," + LICENCENO
            + " text not null," + DATE + " text not null," + LOC
            + " text not null," + SPD + " text not null," + DESCR
            + " text not null," + LAT + " text not null," + LON
            + " text not null);";

    /**
     * Instantiates a new i app db helper.
     *
     * @param context the context
     */
    public DBHelper(Context context) {
        super(context, DB, null, VERSION);
    }

    /* (non-Javadoc)
     * @see android.database.sqlite.SQLiteOpenHelper#onCreate(android.database.sqlite.SQLiteDatabase)
     */
    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_DB);
    }

    /* (non-Javadoc)
     * @see android.database.sqlite.SQLiteOpenHelper#onUpgrade(android.database.sqlite.SQLiteDatabase, int, int)
     */
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldv, int newv) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_CREATE_INC);
        onCreate(db);
    }

    /**
     * Save record.
     *
     * @param cinc the cinc
     */
    public void saveRecord(IncidentsData cinc) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues contVal = new ContentValues();
        contVal.put(CUSID, cinc.getCusId());
        contVal.put(VEH, cinc.getVahicle());
        contVal.put(MCLAUSE, cinc.getMclause());
        contVal.put(SCLAUSE, cinc.getSclause());
        contVal.put(OWN, cinc.getOwner());
        contVal.put(NAME, cinc.getNm());
        contVal.put(LICENCENO, cinc.getLicenNo());
        contVal.put(DATE, cinc.getDt());
        contVal.put(LOC, cinc.getLocation());
        contVal.put(SPD, cinc.getSpeed());
        contVal.put(DESCR, cinc.getDesription());
        contVal.put(LAT, cinc.getLatitude());
        contVal.put(LON, cinc.getLongitude());

        db.insert(TABLE_CREATE_INC, null, contVal);
        db.close();

    }

    /**
     * Delete latest contact.
     */
    public void deleteLatestContact() {
        SQLiteDatabase db = this.getWritableDatabase();

        String selectQuery = "SELECT " + ID + " FROM " + TABLE_CREATE_INC
                + " ORDER BY " + ID + " DESC limit 1";

        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor != null) {
            cursor.moveToFirst();
        }

        db.delete(TABLE_CREATE_INC, ID + " = ?",
                new String[]{String.valueOf(cursor.getString(0))});
        db.close();
        cursor.close();
    }

    /**
     * Retrive record.
     *
     * @param cusid the cusid
     * @return the creates the incident data
     */
    public IncidentsData retriveRecord(String cusid) {

        SQLiteDatabase db = this.getWritableDatabase();

        Cursor cursor = db.query(TABLE_CREATE_INC, new String[]{ID, CUSID,
                        VEH, MCLAUSE, SCLAUSE, OWN, NAME, LICENCENO, DATE, LOC, SPD,
                        DESCR}, CUSID + " =?", new String[]{cusid}, null, null,
                null);
        if (cursor != null) {
            cursor.moveToFirst();
        }
        IncidentsData incidata = new IncidentsData(
                cursor.getString(1), cursor.getString(2), cursor.getString(3),
                cursor.getString(4), cursor.getString(5), cursor.getString(6),
                cursor.getString(7), cursor.getString(8), cursor.getString(9),
                cursor.getString(10), cursor.getString(11),
                cursor.getString(12), cursor.getString(13));
        db.close();
        cursor.close();
        return incidata;

    }

    /**
     * Gets the latest incedent.
     *
     * @return the latest incedent
     */
    public IncidentsData getLatestIncedent() {

        String selectQuery = "SELECT  * FROM " + TABLE_CREATE_INC
                + " ORDER BY " + ID + " DESC limit 1";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor != null) {
            cursor.moveToFirst();
        }
        IncidentsData contact = new IncidentsData(
                cursor.getString(1), cursor.getString(2), cursor.getString(3),
                cursor.getString(4), cursor.getString(5), cursor.getString(6),
                cursor.getString(7), cursor.getString(8), cursor.getString(9),
                cursor.getString(10), cursor.getString(11),
                cursor.getString(12), cursor.getString(13));

        db.close();
        cursor.close();
        return contact;

    }

    /**
     * Gets the all incedents.
     *
     * @return the all incedents
     */
    public List<IncidentsData> getAllIncedents() {

        List<IncidentsData> InciList = new ArrayList<IncidentsData>();

        String selectQuery = "SELECT  * FROM " + TABLE_CREATE_INC;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                IncidentsData inci = new IncidentsData(
                        cursor.getString(1), cursor.getString(2),
                        cursor.getString(3), cursor.getString(4),
                        cursor.getString(5), cursor.getString(6),
                        cursor.getString(7), cursor.getString(8),
                        cursor.getString(9), cursor.getString(10),
                        cursor.getString(11), cursor.getString(12),
                        cursor.getString(13));
                // Adding contact to list
                InciList.add(inci);
            } while (cursor.moveToNext());
        }
        db.close();
        cursor.close();
        // return i list
        return InciList;

    }

}

