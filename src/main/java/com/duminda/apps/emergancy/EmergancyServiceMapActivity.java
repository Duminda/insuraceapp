
package com.duminda.apps.emergancy;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;

import com.duminda.apps.insuranceapp.R;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;

// TODO: Auto-generated Javadoc

/**
 * The Class EmergancyServiceMapActivity.
 */
public class EmergancyServiceMapActivity extends Activity {


    /**
     * The google places.
     */
    GooglePlaces googlePlaces;

    /**
     * The hospitals.
     */
    String hospitals;

    /**
     * The police.
     */
    String police;

    /**
     * The filling stations.
     */
    String fillingStations;

    /**
     * The lat.
     */
    private double lat;

    /**
     * The longi.
     */
    private double longi;


    private GoogleMap map;


    private String HOSPITALS_URL = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=6.934459,79.849910&radius=6000&name=hospital&sensor=false&key=AIzaSyBNO-tKu46u4-nZ4tVrl--Y82HmIOxng6w";
    private String POLICE_URL = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=6.934459,79.849910&radius=6000&name=police&type=police&sensor=false&key=AIzaSyBNO-tKu46u4-nZ4tVrl--Y82HmIOxng6w";
    private String FILLSTATION_URL = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=6.934459,79.849910&radius=6000&name=shed&type=gas&sensor=false&key=AIzaSyBNO-tKu46u4-nZ4tVrl--Y82HmIOxng6w";


    /* (non-Javadoc)
     * @see com.google.android.maps.MapActivity#onCreate(android.os.Bundle)
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.emg_service_map);

        map = ((MapFragment) getFragmentManager().findFragmentById(R.id.emgServmapView))
                .getMap();

		/*makerHospital = new GroundOverlayOptions()
                .image(BitmapDescriptorFactory.fromResource(R.drawable.hospital_marker));


		makerPolice = new GroundOverlayOptions()
				.image(BitmapDescriptorFactory.fromResource(R.drawable.police_marker));


		makerFillStation = new GroundOverlayOptions()
				.image(BitmapDescriptorFactory.fromResource(R.drawable.fillingstation_marker));*/


        googlePlaces = new GooglePlaces();

        try {

            hospitals = googlePlaces.getPlaces(HOSPITALS_URL);
            police = googlePlaces.getPlaces(POLICE_URL);
            fillingStations = googlePlaces.getPlaces(FILLSTATION_URL);


        } catch (Exception e) {
            e.printStackTrace();
        }


        if (!hospitals.isEmpty() && !police.isEmpty()) {
            // loop through all the places
			/*for (Place place : hospitals.results) {
				lat = place.geometry.location.lat; // latitude
				longi = place.geometry.location.lng; // longitude

				makerHospital.position(new LatLng(lat, longi), 8600f, 6500f);

				map.addGroundOverlay(makerHospital);

				Log.v("Place Name : ", place.name + " Lat : " + lat
						+ ", Lon : " + longi);

			}

			for (Place place2 : police.results) {
				lat = place2.geometry.location.lat; // latitude
				longi = place2.geometry.location.lng; // longitude

				makerPolice.position(new LatLng(lat, longi), 8600f, 6500f);

				map.addGroundOverlay(makerPolice);

				Log.v("Place Name : ", place2.name + " Lat : " + lat
						+ ", Lon : " + longi);

			}

			for (Place place3 : fillingStations.results) {
				lat = place3.geometry.location.lat; // latitude
				longi = place3.geometry.location.lng; // longitude

				makerFillStation.position(new LatLng(lat, longi), 8600f, 6500f);

				map.addGroundOverlay(makerFillStation);

				Log.v("Place Name : ", place3.name + " Lat : " + lat
						+ ", Lon : " + longi);
			}*/

        }

    }


    /* (non-Javadoc)
     * @see android.app.Activity#onCreateOptionsMenu(android.view.Menu)
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.emergancy_service_map, menu);
        return true;
    }


}
