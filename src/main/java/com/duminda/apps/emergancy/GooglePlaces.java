package com.duminda.apps.emergancy;

import android.util.Log;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;

public class GooglePlaces {


    public String getPlaces(String url) {

        try {

            URL httpUrl = new URL(url);
            HttpURLConnection conn = (HttpURLConnection) httpUrl.openConnection();
            conn.setRequestMethod("GET");

            String places = conn.getResponseMessage();

            //PlaceList list = request.execute().parseAs(PlaceList.class);
            Log.d("Places Status", "" + places);
            return places;

        } catch (MalformedURLException e) {
            e.printStackTrace();
            return null;
        } catch (ProtocolException e) {
            e.printStackTrace();
            return null;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }


	/*public PlaceList getFillStations() throws Exception{

		try {
			HttpRequestFactory httpRequestFactory = createRequestFactory(HTTP_TRANSPORT);



			HttpRequest request = httpRequestFactory
					.buildGetRequest(new GenericUrl(FILLSTATION_URL));

			PlaceList list = request.execute().parseAs(PlaceList.class);
			Log.d("Places Status", "" + list.status);
			return list;

		} catch (HttpResponseException e) {
            Log.e("Error:", e.getMessage());
            return null;
        }

	}*/
}
