package com.duminda.apps.emergancy;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.ImageView;

import com.duminda.apps.insuranceapp.R;

;

// TODO: Auto-generated Javadoc

/**
 * The Class EmergancyServiceActivity.
 */
public class EmergancyServiceActivity extends Activity implements
        OnTouchListener {

    /**
     * The emgserv.
     */
    private ImageView emgserv;

    /* (non-Javadoc)
     * @see android.app.Activity#onCreate(android.os.Bundle)
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.emergancy_service);

        emgserv = (ImageView) findViewById(R.id.emrg_serv_ivw);
        emgserv.setOnTouchListener(this);

    }

    /* (non-Javadoc)
     * @see android.view.View.OnTouchListener#onTouch(android.view.View, android.view.MotionEvent)
     */
    public boolean onTouch(View v, MotionEvent event) {

        switch (v.getId()) {
            case R.id.emrg_serv_ivw: {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        break;
                    case MotionEvent.ACTION_UP:
                        loadEmgServMap();
                        break;
                    default:
                        break;
                }
                break;
            }
            default:
                break;
        }
        return true;
    }

    /* (non-Javadoc)
     * @see android.app.Activity#onCreateOptionsMenu(android.view.Menu)
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.emergancy_service, menu);
        return true;
    }

    /**
     * Load emg serv map.
     */
    public void loadEmgServMap() {
        Intent summInt = new Intent(this, EmergancyServiceMapActivity.class);
        startActivity(summInt);
    }

}
