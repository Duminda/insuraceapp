package com.duminda.apps.insuranceapp;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.duminda.apps.createincidents.CreateIncidentActivity;
import com.duminda.apps.findagent.FindAgentActivity;
import com.duminda.apps.findgarage.FindGarageActivity;
import com.duminda.apps.myincident.MyIncidentListActivity;
import com.duminda.apps.mypolicy.MyPoliciesActivity;


// TODO: Auto-generated Javadoc

/**
 * The Class HomeActivity.
 */
public class MainActivity extends Activity implements View.OnClickListener {

    /**
     * The global customer id.
     */
    public String globalCUSID;
    /**
     * The Find garage.
     */
    private Button myPolicy_ivw, createInc_ivw, myIncis_ivw, findAgent_ivw,
            callRepr_ivw, findGarage_ivw;
    /**
     * The customer id txt.
     */
    private EditText cidTxt;
    /**
     * The image views array.
     */
    private Button imageArray[] = {myPolicy_ivw, createInc_ivw,
            myIncis_ivw, findAgent_ivw, /*emgService_ivw,*/ callRepr_ivw,
            findGarage_ivw};

    /**
     * Gets the global cusid.
     *
     * @return the global cusid
     */
    public String getGlobalCUSID() {
        return globalCUSID;
    }

    /**
     * Sets the global cusid.
     *
     * @param globalCUSID the new global cusid
     */
    public void setGlobalCUSID(String globalCUSID) {
        this.globalCUSID = globalCUSID;
    }

    /*
     *
     *
     * @see android.app.Activity#onCreate(android.os.Bundle)
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        findViews();
        showCusIdDialog();

    }

    /*
     *
     *
     * @see android.app.Activity#onCreateOptionsMenu(android.view.Menu)
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    /**
     * Find views.
     */
    public void findViews() {

        int i = 0;

        imageArray[0] = (Button) findViewById(R.id.mypolicy);
        imageArray[1] = (Button) findViewById(R.id.createincident);
        imageArray[2] = (Button) findViewById(R.id.myincident);
        imageArray[3] = (Button) findViewById(R.id.findagent);
        //imageArray[4] = (Button) findViewById(R.id.emergancy);
        imageArray[4] = (Button) findViewById(R.id.callrep);
        imageArray[5] = (Button) findViewById(R.id.findgarage);

        for (i = 0; i < imageArray.length; i++) {
            imageArray[i].setOnClickListener(this);
        }
    }

    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo ni = cm.getActiveNetworkInfo();
        if (ni != null && ni.isConnected()) {
            return true;
        } else
            return false;
    }

    private void showNetworkErrDialog() {
        AlertDialog alertDialog = new AlertDialog.Builder(MainActivity.this).create();
        alertDialog.setTitle("Network Problem");
        alertDialog.setMessage("Network connection not available!");
        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        alertDialog.show();
    }

    public void onClick(View v) {

        if(isNetworkConnected()) {

            switch (v.getId()) {
                case R.id.mypolicy: {
                    startMyPolicies();
                }
                break;
                case R.id.createincident: {
                    // Create Incident
                    startCreateInci();
                }
                break;
                case R.id.myincident: {
                    // My Incident
                    startMyIncidents();

                }
                break;
                case R.id.findagent: {
                    // Find Agent
                    startFindAgents();

                }
                break;
            /*case R.id.emergancy: {
                // Emergency Service
                startEmergancyService();

            }
            break;*/
                case R.id.callrep: {
                    // Call Representative
                    startCallrep();

                }
                break;
                case R.id.findgarage: {
                    // Find Garage
                    startFindGarage();
                }
                default:
                    break;
            }
        } else
            showNetworkErrDialog();
    }

    /**
     * Start my policies intent.
     */
    public void startMyPolicies() {

        Intent myPolicyIntent = new Intent(this, MyPoliciesActivity.class);
        myPolicyIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        myPolicyIntent.putExtra("globalCusID", globalCUSID);
        startActivity(myPolicyIntent);
    }

    /**
     * Start create incident intent.
     */
    public void startCreateInci() {

        Intent createinc = new Intent(this, CreateIncidentActivity.class);
        createinc.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        createinc.putExtra("globalCusID", globalCUSID);
        startActivity(createinc);
    }

    /**
     * Start my incidents intent.
     */
    public void startMyIncidents() {
        Intent summInt = new Intent(this, MyIncidentListActivity.class);
        startActivity(summInt);
    }

    /**
     * Start find agents intent.
     */
    public void startFindAgents() {
        Intent summInt = new Intent(this, FindAgentActivity.class);
        startActivity(summInt);
    }

    /**
     * Start emergancy service intent.
     */
   /* public void startEmergancyService() {
        Intent summInt = new Intent(this, EmergancyServiceActivity.class);
        startActivity(summInt);*//*

        *//*Intent summInt = new Intent(this, EmergancyServiceMapActivity.class);
        startActivity(summInt);
    }*/

    /**
     * Start call representative intent.
     */
    public void startCallrep() {
        Intent callIntent = new Intent(Intent.ACTION_CALL);
        callIntent.setData(Uri.parse("tel:0779906999"));
        startActivity(callIntent);
    }

    /**
     * Start find garage intent.
     */
    public void startFindGarage() {
        Intent garageInt = new Intent(this, FindGarageActivity.class);
        startActivity(garageInt);
    }

    /**
     * Show cus id dialog.
     *
     * @return the string
     */
    String showCusIdDialog() {

        AlertDialog.Builder helpBuilder = new AlertDialog.Builder(this);
        helpBuilder.setTitle("Customer ID");
        helpBuilder.setMessage("Please enter your Customer ID");

        cidTxt = new EditText(this);

        cidTxt.setSingleLine();
        cidTxt.setText("");

        helpBuilder.setView(cidTxt);

        helpBuilder.setPositiveButton("Ok",
                new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dlg, int arg1) {
                        globalCUSID = cidTxt.getText().toString();

                    }
                });

        AlertDialog helpDialog = helpBuilder.create();
        helpDialog.show();

        return globalCUSID;
    }

}
