package com.duminda.apps.findagent;

import android.app.Activity;
import android.app.ProgressDialog;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Menu;

import com.duminda.apps.common.JSONParser;
import com.duminda.apps.insuranceapp.R;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

// TODO: Auto-generated Javadoc

/**
 * The Class FindAgentActivity.
 */
public class FindAgentActivity extends Activity {

    /** The mview. */
    //private MapView mview;

    /**
     * The pdialog.
     */
    private ProgressDialog pdialog;

    /**
     * The jparser.
     */
    JSONParser jparser = new JSONParser();

    /**
     * The Constant TAG_SUCCESS.
     */
    private static final String TAG_SUCCESS = "success";

    /**
     * The Constant TAG_AGENTS.
     */
    private static final String TAG_AGENTS = "Agents";

    /**
     * The Constant TAG_AGENT_LAT.
     */
    private static final String TAG_AGENT_LAT = "Latitude";

    /**
     * The Constant TAG_AGENT_LON.
     */
    private static final String TAG_AGENT_LON = "Longitude";

    /**
     * The Constant TAG_AGENT_LOC.
     */
    private static final String TAG_AGENT_LOC = "Location";

    /**
     * The Constant TAG_AGENT_TP.
     */
    private static final String TAG_AGENT_TP = "TP";

    /**
     * The agents_records_url.
     */
    private String agents_records_url = "";

    /**
     * The agents.
     */
    JSONArray agents = null;

    /**
     * The agent_lat.
     */
    private String agent_lat;

    /**
     * The agent_lon.
     */
    private String agent_lon;

    /**
     * The agent_loc.
     */
    private String agent_loc;

    /**
     * The agent_tp.
     */
    private String agent_tp;

    /**
     * The gp.
     */
    private LatLng[] gp = new LatLng[10];

    /**
     * The tp.
     */
    private String[] tp = new String[10];

    /**
     * The location.
     */
    private String[] location = new String[10];

    private MarkerOptions marker;

    private GoogleMap map;

    /* (non-Javadoc)
     * @see com.google.android.maps.MapActivity#onCreate(android.os.Bundle)
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.find_agent);

        agents_records_url = getResources().getString(R.string.hostAddress)+"index_getagents.php";

        map = ((MapFragment) getFragmentManager().findFragmentById(R.id.findAgentMapView))
                .getMap();

        new LoadAgentsData().execute();

    }

   /* @Override
    public void onMapReady(GoogleMap map) {

        try {
            Uri.Builder builder = new Uri.Builder();

            JSONObject json = jparser.makeHttpRequest(agents_records_url,
                    "GET", builder);

            Log.v("JSON agent data : ", json.toString());

            // Checking for SUCCESS TAG
            int success = json.getInt(TAG_SUCCESS);

            if (success == 1) {

                agents = json.getJSONArray(TAG_AGENTS);

                // looping through All Products
                for (int i = 0; i < agents.length(); i++) {
                    JSONObject c = agents.getJSONObject(i);

                    // Storing each json item in variable
                    agent_lat = c.getString(TAG_AGENT_LAT);
                    agent_lon = c.getString(TAG_AGENT_LON);
                    agent_loc = c.getString(TAG_AGENT_LOC);
                    agent_tp = c.getString(TAG_AGENT_TP);

                    double lat = Double.parseDouble(agent_lat);
                    double lon = Double.parseDouble(agent_lon);

                    gp[i] = new LatLng(lat, lon);
                    tp[i] = agent_tp;
                    location[i] = agent_loc;

                    //marker = new MarkerOptions().position(gp[i]);

                    map.addMarker(new MarkerOptions()
                            .position(gp[i]));

                    //marker.icon(BitmapDescriptorFactory.fromResource(R.drawable.marker));
                    //map.addMarker(marker);
                }

            } else {

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }*/

    /* (non-Javadoc)
     * @see android.app.Activity#onCreateOptionsMenu(android.view.Menu)
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.find_agent, menu);
        return true;
    }


    /**
     * The Class LoadAgentsData.
     */
    class LoadAgentsData extends AsyncTask<String, String, String> {

        /* (non-Javadoc)
         * @see android.os.AsyncTask#onPreExecute()
         */
        @Override
        protected void onPreExecute() {

            super.onPreExecute();

            pdialog = new ProgressDialog(FindAgentActivity.this);
            pdialog.setMessage("Loading Agents ....");
            pdialog.setIndeterminate(false);
            pdialog.setCancelable(false);
            pdialog.show();
        }

        /* (non-Javadoc)
         * @see android.os.AsyncTask#doInBackground(Params[])
         */
        @Override
        protected String doInBackground(String... params) {

            try {


                Uri.Builder builder = new Uri.Builder();

                JSONObject json = jparser.makeHttpRequest(agents_records_url,
                        "GET", builder);

                //Log.v("JSON agent data : ", json.toString());

                // Checking for SUCCESS TAG
                int success = json.getInt(TAG_SUCCESS);

                if (success == 1) {

                    agents = json.getJSONArray(TAG_AGENTS);

                    // looping through All Products


                } else {

                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        /* (non-Javadoc)
         * @see android.os.AsyncTask#onPostExecute(java.lang.Object)
         */
        protected void onPostExecute(String file_url) {
            // dismiss the dialog after getting all products


            for (int i = 0; i < agents.length(); i++) {

                try {
                    JSONObject c = agents.getJSONObject(i);

                    // Storing each json item in variable
                    agent_lat = c.getString(TAG_AGENT_LAT);
                    agent_lon = c.getString(TAG_AGENT_LON);
                    agent_loc = c.getString(TAG_AGENT_LOC);
                    agent_tp = c.getString(TAG_AGENT_TP);

                    double lat = Double.parseDouble(agent_lat);
                    double lon = Double.parseDouble(agent_lon);

                    gp[i] = new LatLng(lat, lon);
                    tp[i] = agent_tp;
                    location[i] = agent_loc;

                    if (map != null) {
                        Marker agent = map.addMarker(new MarkerOptions().position(gp[i]));


                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
            map.moveCamera(CameraUpdateFactory.newLatLngZoom(gp[0], 15));

            pdialog.dismiss();
        }

    }

}
