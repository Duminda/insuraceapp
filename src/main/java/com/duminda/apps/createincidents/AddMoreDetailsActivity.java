
package com.duminda.apps.createincidents;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.duminda.apps.common.DBHelper;
import com.duminda.apps.common.JSONParser;
import com.duminda.apps.insuranceapp.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * The Class AddMoreDetailsActivity.
 */
public class AddMoreDetailsActivity extends Activity implements View.OnClickListener {

    /**
     * The myvehicle.
     */
    private Button myvehicle;

    /**
     * The passengers.
     */
    private Button passengers;

    /**
     * The thrdparty.
     */
    private Button thrdparty;

    /**
     * The pedestr.
     */
    private Button pedestr;

    /**
     * The property.
     */
    private Button property;

    /**
     * The witnes.
     */
    private Button witnes;

    /**
     * The vw1.
     */
    View vw1;

    /**
     * The vw2.
     */
    View vw2;

    /**
     * The summary.
     */
    private Button summary;

    /**
     * The savesubmitlater.
     */
    private Button savesubmitlater;

    /**
     * The submit.
     */
    private Button submit;

    /**
     * The incident values.
     */
    String[] incidentValues = new String[13];
    // private String gCusid;

    /**
     * The pdialog.
     */
    private ProgressDialog pdialog;

    /**
     * The jparser3.
     */
    JSONParser jparser3 = new JSONParser();

    /**
     * The incidents.
     */
    JSONArray incidents = null;

    /**
     * The create_incident_url.
     */
    private String create_incident_url = "";

    /**
     * The Constant TAG_SUCCESS.
     */
    private static final String TAG_SUCCESS = "success";

    /**
     * The Constant TAG_CUSID.
     */
    private static final String TAG_CUSID = "CusID";

    /**
     * The Constant TAG_VEHICLE.
     */
    private static final String TAG_VEHICLE = "Vehicle";

    /**
     * The Constant TAG_MAIN_CLAUSE.
     */
    private static final String TAG_MAIN_CLAUSE = "MainCause";

    /**
     * The Constant TAG_SUB_CLAUSE.
     */
    private static final String TAG_SUB_CLAUSE = "SubCause";

    /**
     * The Constant TAG_OWNER.
     */
    private static final String TAG_OWNER = "Owner";

    /**
     * The Constant TAG_NAME.
     */
    private static final String TAG_NAME = "Name";

    /**
     * The Constant TAG_LICENCE_NO.
     */
    private static final String TAG_LICENCE_NO = "LicenceNo";

    /**
     * The Constant TAG_DATE.
     */
    private static final String TAG_DATE = "Date";

    /**
     * The Constant TAG_LOCATION.
     */
    private static final String TAG_LOCATION = "Location";

    /**
     * The Constant TAG_SPPEED.
     */
    private static final String TAG_SPPEED = "Speed";

    /**
     * The Constant TAG_DESCRIPTION.
     */
    private static final String TAG_DESCRIPTION = "Description";

    /**
     * The Constant TAG_LAT.
     */
    private static final String TAG_LAT = "Latitude";

    /**
     * The Constant TAG_LON.
     */
    private static final String TAG_LON = "Longitude";

    /**
     * The Constant TAG_STATUS.
     */
    private static final String TAG_STATUS = "Status";

    /* (non-Javadoc)
     * @see android.app.Activity#onCreate(android.os.Bundle)
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_more_details);

        myvehicle = (Button) findViewById(R.id.myvehi);
        passengers = (Button) findViewById(R.id.passengers);
        thrdparty = (Button) findViewById(R.id.thirdparty);
        pedestr = (Button) findViewById(R.id.pedastrians);
        property = (Button) findViewById(R.id.property);
        witnes = (Button) findViewById(R.id.witness);

        create_incident_url = getResources().getString(R.string.hostAddress)+"index_saveIncident.php";

        savesubmitlater = (Button) findViewById(R.id.save_submit_ivw);
        submit = (Button) findViewById(R.id.submit_ivw);
        summary = (Button) findViewById(R.id.viewsummary_ivw);

        myvehicle.setOnClickListener(this);
        passengers.setOnClickListener(this);
        thrdparty.setOnClickListener(this);
        pedestr.setOnClickListener(this);
        property.setOnClickListener(this);
        witnes.setOnClickListener(this);

        summary.setOnClickListener(this);
        submit.setOnClickListener(this);
        savesubmitlater.setOnClickListener(this);

    }

    /* (non-Javadoc)
     * @see android.view.View.OnTouchListener#onTouch(android.view.View, android.view.MotionEvent)
     */
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.myvehi: {
                startmyVehicle();


                break;
            }
            case R.id.passengers: {
                startPassengers();

                break;
            }
            case R.id.thirdparty: {
                startThirdParty();

                break;
            }
            case R.id.pedastrians: {
                startPedestrians();

                break;
            }
            case R.id.property: {
                startProperty();

                break;
            }
            case R.id.witness: {
                startWitnasses();

                break;
            }
            case R.id.submit_ivw: {
                getLatestRecord();
                new SaveIncident().execute();

                break;
            }
            case R.id.save_submit_ivw: {
                Bundle extras = getIntent().getExtras();
                incidentValues = extras.getStringArray("IncidentDataArray");
                saveRecord(incidentValues);


                break;
            }
            case R.id.viewsummary_ivw: {
                launchSummary();

                break;
            }
            default:
                break;
        }
    }

    /**
     * Startmy vehicle.
     */
    private void startmyVehicle() {
        Intent intnt = new Intent(this, MyVehiclesActivity.class);
        startActivity(intnt);

    }

    /**
     * Start passengers.
     */
    private void startPassengers() {
        Intent intnt = new Intent(this, PassengersActivity.class);
        startActivity(intnt);
    }

    /**
     * Start third party.
     */
    private void startThirdParty() {
        Intent intnt = new Intent(this, ThirdPartyActivity.class);
        startActivity(intnt);
    }

    /**
     * Start pedestrians.
     */
    private void startPedestrians() {
        Intent intnt = new Intent(this, PedestriansActivity.class);
        startActivity(intnt);
    }

    /**
     * Start property.
     */
    private void startProperty() {
        Intent intnt = new Intent(this, PropertyActivity.class);
        startActivity(intnt);
    }

    /**
     * Start witnasses.
     */
    private void startWitnasses() {
        Intent intnt = new Intent(this, WitnessesActivity.class);
        startActivity(intnt);
    }

    /**
     * Save record.
     *
     * @param vals the vals
     */
    void saveRecord(String[] vals) {

        DBHelper dbhelper = new DBHelper(this);

        dbhelper.saveRecord(new IncidentsData(vals));
    }

    /**
     * Gets the record.
     *
     * @param cusid the cusid
     * @return the record
     */
    String[] getRecord(String cusid) {

        DBHelper dbhelper = new DBHelper(this);

        IncidentsData contacts = dbhelper.retriveRecord(cusid);
        incidentValues[0] = contacts.getCusId();
        incidentValues[1] = contacts.getVahicle();
        incidentValues[2] = contacts.getMclause();
        incidentValues[3] = contacts.getSclause();
        incidentValues[4] = contacts.getOwner();
        incidentValues[5] = contacts.getNm();
        incidentValues[6] = contacts.getLicenNo();
        incidentValues[7] = contacts.getDt();
        incidentValues[8] = contacts.getLocation();
        incidentValues[9] = contacts.getSpeed();
        incidentValues[10] = contacts.getDesription();
        incidentValues[11] = contacts.getLatitude();
        incidentValues[12] = contacts.getLongitude();

        return incidentValues;
    }

    /**
     * Gets the latest record.
     *
     * @return the latest record
     */
    String[] getLatestRecord() {

        DBHelper db = new DBHelper(this);
        IncidentsData contacts = db.getLatestIncedent();

        incidentValues[0] = contacts.getCusId();
        incidentValues[1] = contacts.getVahicle();
        incidentValues[2] = contacts.getMclause();
        incidentValues[3] = contacts.getSclause();
        incidentValues[4] = contacts.getOwner();
        incidentValues[5] = contacts.getNm();
        incidentValues[6] = contacts.getLicenNo();
        incidentValues[7] = contacts.getDt();
        incidentValues[8] = contacts.getLocation();
        incidentValues[9] = contacts.getSpeed();
        incidentValues[10] = contacts.getDesription();
        incidentValues[11] = contacts.getLatitude();
        incidentValues[12] = contacts.getLongitude();

        return incidentValues;

    }

    /**
     * Launch summary.
     */
    public void launchSummary() {

        getLatestRecord();
        Intent summInt = new Intent(this, SummaryIncidentActivity.class);
        summInt.putExtra("summaryData", incidentValues);
        startActivity(summInt);
    }

    /**
     * The Class SaveIncident.
     */
    class SaveIncident extends AsyncTask<String, String, String> {

        /* (non-Javadoc)
         * @see android.os.AsyncTask#onPreExecute()
         */
        @Override
        protected void onPreExecute() {

            super.onPreExecute();

            pdialog = new ProgressDialog(AddMoreDetailsActivity.this);
            pdialog.setMessage("Saving Incident....");
            pdialog.setIndeterminate(false);
            pdialog.setCancelable(false);
            pdialog.show();
        }

        /* (non-Javadoc)
         * @see android.os.AsyncTask#doInBackground(Params[])
         */
        @Override
        protected String doInBackground(String... arg0) {


            try {

                Uri.Builder builder = new Uri.Builder();

                builder.appendQueryParameter(TAG_CUSID, incidentValues[0]);
                builder.appendQueryParameter(TAG_VEHICLE, incidentValues[1]);
                builder.appendQueryParameter(TAG_MAIN_CLAUSE, incidentValues[2]);
                builder.appendQueryParameter(TAG_SUB_CLAUSE, incidentValues[3]);
                builder.appendQueryParameter(TAG_OWNER, incidentValues[4]);
                builder.appendQueryParameter(TAG_NAME, incidentValues[5]);
                builder.appendQueryParameter(TAG_LICENCE_NO, incidentValues[6]);
                builder.appendQueryParameter(TAG_DATE, incidentValues[7]);
                builder.appendQueryParameter(TAG_LOCATION, incidentValues[8]);
                builder.appendQueryParameter(TAG_SPPEED, incidentValues[9]);
                builder.appendQueryParameter(TAG_DESCRIPTION, incidentValues[10]);
                builder.appendQueryParameter(TAG_LAT, incidentValues[11]);
                builder.appendQueryParameter(TAG_LON, incidentValues[12]);
                builder.appendQueryParameter(TAG_STATUS, "Pending");


                JSONObject json = jparser3.makeHttpRequest(
                        create_incident_url, "POST", builder);

                //Log.v("JSON incident data : ", json.toString());

                // Checking for SUCCESS TAG
                int success = json.getInt(TAG_SUCCESS);

                if (success == 1) {
                    // incidents saved

                } else {
                    // save failed

                }
            } catch (JSONException e) {
                e.printStackTrace();
            }


            return null;
        }

        /* (non-Javadoc)
         * @see android.os.AsyncTask#onPostExecute(java.lang.Object)
         */
        protected void onPostExecute(String file_url) {
            // dismiss the dialog after saving incidents
            pdialog.dismiss();

        }

    }

}
