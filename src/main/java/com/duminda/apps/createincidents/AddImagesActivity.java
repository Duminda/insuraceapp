
package com.duminda.apps.createincidents;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentResolver;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.duminda.apps.common.JSONParser;
import com.duminda.apps.insuranceapp.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

/**
 * The Class AddImagesActivity.
 */
public class AddImagesActivity extends Activity implements View.OnClickListener {

    /**
     * The type spinner.
     */
    private Spinner typeSpinner;

    /**
     * The type items.
     */
    private ArrayList typeItems = new ArrayList();

    /**
     * The add capture.
     */
    private ImageView addCapture;

    /**
     * The save image.
     */
    private Button saveImage;

    /**
     * The notes.
     */
    private EditText notes;

    /**
     * The Constant SELECT_PICTURE.
     */
    private static final int SELECT_PICTURE = 1;

    /**
     * The Constant CAMERA_PIC_REQUEST.
     */
    private static final int CAMERA_PIC_REQUEST = 0;

    /**
     * The Constant TAG_SUCCESS.
     */
    private static final String TAG_SUCCESS = "success";

    /**
     * The Constant TAG_VEHI_TYPE.
     */
    private static final String TAG_VEHI_TYPE = "Type";

    /**
     * The Constant TAG_VEHI_NOTES.
     */
    private static final String TAG_VEHI_NOTES = "Notes";

    /**
     * The Constant TAG_VEHI_IMAGE.
     */
    private static final String TAG_VEHI_IMAGE = "Image";

    /**
     * The image uri.
     */
    private Uri imageUri;

    /**
     * The bitmap.
     */
    private Bitmap bitmap;

    /**
     * The vimage.
     */
    private String vimage;

    /**
     * The vtype.
     */
    private String vtype;

    /**
     * The vnotes.
     */
    private String vnotes;

    /**
     * The json parser.
     */
    JSONParser jsonParser = new JSONParser();

    /**
     * The Constant image_upload_url.
     */
    private String image_upload_url = "";

    /* (non-Javadoc)
     * @see android.app.Activity#onCreate(android.os.Bundle)
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_images);

        image_upload_url = getResources().getString(R.string.hostAddress)+"index_imageupload.php";

        typeItems.add("My Vehicle");
        typeItems.add("Passengers");
        typeItems.add("3rd Party");
        typeItems.add("Pedestrians");
        typeItems.add("Property");
        typeItems.add("Witnesses");

        typeSpinner = (Spinner) findViewById(R.id.type_spinner);

        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(
                this, android.R.layout.simple_spinner_dropdown_item, typeItems);
        typeSpinner.setAdapter(spinnerArrayAdapter);

        addCapture = (ImageView) findViewById(R.id.add_capture_img_ivw);
        saveImage = (Button) findViewById(R.id.save_image_ivw);
        notes = (EditText) findViewById(R.id.notes_txt);

        addCapture.setOnClickListener(this);
        saveImage.setOnClickListener(this);
    }

    /* (non-Javadoc)
     * @see android.view.View.OnTouchListener#onTouch(android.view.View, android.view.MotionEvent)
     */
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.add_capture_img_ivw:
                launchAddOrCaptureDialog();
                break;
            case R.id.save_image_ivw: {
                //saveImage.setImageResource(R.drawable.save);
                vnotes = notes.getText().toString();
                vtype = typeSpinner.getSelectedItem().toString();
                new SaveImage().execute();// save image
                Toast.makeText(this, "Saved Successfully ! ", Toast.LENGTH_LONG)
                        .show();
            }
            break;
            default:
                break;
        }
    }

    /* (non-Javadoc)
     * @see android.app.Activity#onCreateOptionsMenu(android.view.Menu)
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.add_images, menu);
        return true;
    }

    /**
     * Launch add or capture dialog.
     */
    public void launchAddOrCaptureDialog() {

        AlertDialog.Builder setImageDlg = new AlertDialog.Builder(
                AddImagesActivity.this);
        setImageDlg.setTitle("Change Picture :");
        final CharSequence[] opsChars = {
                getResources().getString(R.string.take_pic),
                getResources().getString(R.string.select_pic)};

        setImageDlg.setItems(opsChars,

                new android.content.DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int which) {
                        if (which == 0) {
                            Intent cameraIntent = new Intent(
                                    android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                            File photo = new File(Environment
                                    .getExternalStorageDirectory(), "Pic.jpg");
                            cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT,
                                    Uri.fromFile(photo));
                            imageUri = Uri.fromFile(photo);
                            startActivityForResult(cameraIntent,
                                    CAMERA_PIC_REQUEST);

                        } else if (which == 1) {
                            Intent intent = new Intent();
                            intent.setType("image/*");
                            intent.setAction(Intent.ACTION_GET_CONTENT);
                            startActivityForResult(Intent.createChooser(
                                            intent,
                                            getResources().getString(
                                                    R.string.pick_gallary)),
                                    SELECT_PICTURE);

                        }
                        dialog.dismiss();
                    }
                });

        setImageDlg.create();
        setImageDlg.show();
    }

    /* (non-Javadoc)
     * @see android.app.Activity#onActivityResult(int, int, android.content.Intent)
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == SELECT_PICTURE && resultCode == Activity.RESULT_OK)

            try {
                InputStream stream = getContentResolver().openInputStream(
                        data.getData());
                bitmap = BitmapFactory.decodeStream(stream);
                stream.close();
                addCapture.setImageBitmap(Bitmap.createScaledBitmap(bitmap,
                        500, 500, false));

                ByteArrayOutputStream bao = new ByteArrayOutputStream();

                bitmap.compress(Bitmap.CompressFormat.JPEG, 90, bao);

                byte[] ba = bao.toByteArray();

                vimage = Base64.encodeToString(ba, 0);

            } catch (FileNotFoundException e) {

                e.printStackTrace();
            } catch (IOException e) {

                e.printStackTrace();
            }

        if (requestCode == CAMERA_PIC_REQUEST
                && resultCode == Activity.RESULT_OK) {
            getContentResolver().notifyChange(imageUri, null);

            ContentResolver cr = getContentResolver();

            Bitmap bitmap;
            try {
                bitmap = android.provider.MediaStore.Images.Media.getBitmap(cr,
                        imageUri);

                addCapture.setImageBitmap(Bitmap.createScaledBitmap(bitmap,
                        300, 300, false));

                ByteArrayOutputStream bao = new ByteArrayOutputStream();

                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bao);

                byte[] ba = bao.toByteArray();

                vimage = Base64.encodeToString(ba, 0);

            } catch (Exception e) {

                Log.e("Camera", e.toString());
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    /**
     * The Class SaveImage.
     */
    class SaveImage extends AsyncTask<String, String, String> {

        /* (non-Javadoc)
         * @see android.os.AsyncTask#onPostExecute(java.lang.Object)
         */
        @Override
        protected void onPostExecute(String result) {

            super.onPostExecute(result);
        }

        /* (non-Javadoc)
         * @see android.os.AsyncTask#onPreExecute()
         */
        @Override
        protected void onPreExecute() {

            super.onPreExecute();
        }

        /* (non-Javadoc)
         * @see android.os.AsyncTask#doInBackground(Params[])
         */
        @Override
        protected String doInBackground(String... arg0) {

            Uri.Builder builder = new Uri.Builder();

            builder.appendQueryParameter(TAG_VEHI_TYPE, vtype);
            builder.appendQueryParameter(TAG_VEHI_NOTES, vnotes);
            builder.appendQueryParameter(TAG_VEHI_IMAGE, vimage);


            JSONObject json2 = jsonParser.makeHttpRequest(image_upload_url,
                    "POST", builder);
            //Log.d("Image data", json2.toString());
            //Log.i("Patameters", "[" + builder + "]");

            try {
                int success = json2.getInt(TAG_SUCCESS);

                if (success == 1) {
                    // successfully updated
                    Intent i = getIntent();
                    // send result code 100 to notify about product update
                    setResult(100, i);
                    finish();
                } else {
                    // failed to update product
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }
    }

}
