package com.duminda.apps.createincidents;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.duminda.apps.common.JSONParser;
import com.duminda.apps.insuranceapp.R;

import org.json.JSONException;
import org.json.JSONObject;

public class PassengersActivity extends Activity implements View.OnClickListener {


    private ProgressDialog pDialog;

    private static final String TAG_ID = "ID";
    private static final String TAG_TYPE = "Type";
    private static final String TAG_OWNER = "Owner";
    private static final String TAG_CONTACT = "Contact";
    private static final String TAG_ADDRESS = "Address";
    private static final String TAG_SUCCESS = "success";

    JSONParser jsonParser = new JSONParser();
    private String critical_info_url = "";



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.passengers);

        critical_info_url = getResources().getString(R.string.hostAddress)+"index_saveCriticalInfo.php";

        Button launchAddImages = (Button) findViewById(R.id.add_img_ivw);
        Button saveMyVehicle = (Button) findViewById(R.id.save_passeng_ivw);

        launchAddImages.setOnClickListener(this);
        saveMyVehicle.setOnClickListener(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.passengers, menu);
        return true;
    }

    public void lauchAddImages() {
        Intent intn = new Intent(this, AddImagesActivity.class);
        startActivity(intn);
    }

    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.add_img_ivw:
                lauchAddImages();
                break;
            case R.id.save_passeng_ivw: {
                new SavePassengerData().execute();

                break;
            }
            default:
                break;
        }
    }

    class SavePassengerData extends AsyncTask<String, String, String> {

        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(PassengersActivity.this);
            pDialog.setMessage("Saving ...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }

        @Override
        protected String doInBackground(String... arg) {

            EditText idtxt;
            EditText ownertxt;
            EditText contacttxt;
            EditText addresstxt;
            String id;
            String type;
            String owner;
            String contact;
            String address;

            idtxt = (EditText) findViewById(R.id.pesseng_id_txt);
            ownertxt = (EditText) findViewById(R.id.passeng_name_txt);
            contacttxt = (EditText) findViewById(R.id.pesseng_contct_no_txt);
            addresstxt = (EditText) findViewById(R.id.pesseng_addr_txt);

            id = idtxt.getText().toString();
            type = "Passenger";
            owner = ownertxt.getText().toString();
            contact = contacttxt.getText().toString();
            address = addresstxt.getText().toString();

            Uri.Builder builder = new Uri.Builder();

            builder.appendQueryParameter(TAG_ID, id);
            builder.appendQueryParameter(TAG_TYPE, type);
            builder.appendQueryParameter(TAG_OWNER, owner);
            builder.appendQueryParameter(TAG_CONTACT, contact);
            builder.appendQueryParameter(TAG_ADDRESS, address);

            JSONObject json2 = jsonParser.makeHttpRequest(critical_info_url,
                    "POST", builder);
            Log.d("Save Critical", json2.toString());
            Log.i("Patameters", "[" + builder + "]");

            try {
                int success = json2.getInt(TAG_SUCCESS);

                if (success == 1) {
                    // successfully updated
                    Intent i = getIntent();
                    // send result code 100 to notify about product update
                    setResult(100, i);
                    finish();
                } else {
                    // failed to update product
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        protected void onPostExecute(String file_url) {

            pDialog.dismiss();

            //finish();

        }

    }
}
