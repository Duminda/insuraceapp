package com.duminda.apps.createincidents;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.widget.TextView;

import com.duminda.apps.insuranceapp.R;

public class SummaryIncidentActivity extends Activity {

    String[] summaryValues = new String[13];

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.summary_incident);

        Bundle extras = getIntent().getExtras();
        summaryValues = extras.getStringArray("summaryData");

        TextView vehi = (TextView) findViewById(R.id.vehi_sum_tvw);
        TextView mclause = (TextView) findViewById(R.id.mainclause_sum_tvw);
        TextView sclause = (TextView) findViewById(R.id.subclause_sum_tvw);
        TextView name = (TextView) findViewById(R.id.name_sum_tvw);
        TextView licno = (TextView) findViewById(R.id.licno_sum_tvw);
        TextView date = (TextView) findViewById(R.id.date_sum_tvw);
        TextView loc = (TextView) findViewById(R.id.loc_sum_tvw);
        TextView speed = (TextView) findViewById(R.id.speed_sum_tvw);
        TextView descr = (TextView) findViewById(R.id.descr_sum_tvw);

        vehi.setText(summaryValues[1]);
        mclause.setText(summaryValues[2]);
        sclause.setText(summaryValues[3]);
        name.setText(summaryValues[5]);
        licno.setText(summaryValues[6]);
        date.setText(summaryValues[7]);
        loc.setText(summaryValues[8]);
        speed.setText(summaryValues[9]);
        descr.setText(summaryValues[10]);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.summary_incident, menu);
        return true;
    }
}
