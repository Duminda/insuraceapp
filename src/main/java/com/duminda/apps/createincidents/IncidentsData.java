package com.duminda.apps.createincidents;

/**
 * Created by duminda on 5/26/15.
 */
public class IncidentsData {


    private String cusId, vahicle, mclause, sclause, own, nm, licno, dt, loc, spd, desr, lat, lon;

    public IncidentsData(String cusId, String vah, String mclause,
                         String sclause, String own, String nm, String licno, String dt,
                         String loc, String spd, String desr, String lat, String lon) {
        super();
        this.cusId = cusId;
        this.vahicle = vah;
        this.mclause = mclause;
        this.sclause = sclause;
        this.own = own;
        this.nm = nm;
        this.licno = licno;
        this.dt = dt;
        this.loc = loc;
        this.spd = spd;
        this.desr = desr;
        this.lat = lat;
        this.lon = lon;
    }

    public IncidentsData(String[] vals) {
        this.cusId = vals[0];
        this.vahicle = vals[1];
        this.mclause = vals[2];
        this.sclause = vals[3];
        this.own = vals[4];
        this.nm = vals[5];
        this.licno = vals[6];
        this.dt = vals[7];
        this.loc = vals[8];
        this.spd = vals[9];
        this.desr = vals[10];
        this.lat = vals[11];
        this.lon = vals[12];
    }

    public String getLatitude() {
        return lat;
    }

    public void setLatitude(String lat) {
        this.lat = lat;
    }

    public String getLongitude() {
        return lon;
    }

    public void setLongitude(String lon) {
        this.lon = lon;
    }

    public String getCusId() {
        return cusId;
    }

    public void setCusId(String cusid) {
        this.cusId = cusid;
    }

    public String getVahicle() {
        return vahicle;
    }

    public void setVahicle(String vah) {
        this.vahicle = vah;
    }

    public String getMclause() {
        return mclause;
    }

    public void setMclause(String mclause) {
        this.mclause = mclause;
    }

    public String getSclause() {
        return sclause;
    }

    public void setSclause(String sclause) {
        this.sclause = sclause;
    }

    public String getOwner() {
        return own;
    }

    public void setOwner(String own) {
        this.own = own;
    }

    public String getNm() {
        return nm;
    }

    public void setNm(String nm) {
        this.nm = nm;
    }

    public String getLicenNo() {
        return licno;
    }

    public void setLicenNo(String licno) {
        this.licno = licno;
    }

    public String getDt() {
        return dt;
    }

    public void setDt(String dt) {
        this.dt = dt;
    }

    public String getLocation() {
        return loc;
    }

    public void setLocation(String loc) {
        this.loc = loc;
    }

    public String getSpeed() {
        return spd;
    }

    public void setSpeed(String spd) {
        this.spd = spd;
    }

    public String getDesription() {
        return desr;
    }

    public void setDesription(String desr) {
        this.desr = desr;
    }

}
