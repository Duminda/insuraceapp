
package com.duminda.apps.createincidents;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;

import com.duminda.apps.common.JSONParser;
import com.duminda.apps.insuranceapp.R;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.GroundOverlayOptions;
import com.google.android.gms.maps.model.LatLng;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;

// TODO: Auto-generated Javadoc

/**
 * The Class CreateIncidentActivity.
 */
@SuppressWarnings("deprecation")
public class CreateIncidentActivity extends Activity implements
        OnClickListener, OnCheckedChangeListener {

    /**
     * The vehicle.
     */
    private Spinner vehicle;

    /**
     * The main_cause.
     */
    private Spinner main_cause;

    /**
     * The sub_cause.
     */
    private Spinner sub_cause;

    /**
     * The owner.
     */
    private CheckBox owner;

    /**
     * The name.
     */
    private EditText name;

    /**
     * The licence_no.
     */
    private EditText licence_no;

    /**
     * The date.
     */
    private EditText date;

    /**
     * The get_curr_loc.
     */
    private CheckBox get_curr_loc;

    /**
     * The location.
     */
    private EditText location;

    /**
     * The speed.
     */
    private EditText speed;

    /**
     * The description.
     */
    private EditText description;

    /**
     * The next.
     */
    private Button next;

    /**
     * The date_pick.
     */
    private Button date_pick;

    /**
     * The map_load.
     */
    private Button map_load;

    /**
     * The mapload lay.
     */
    private LinearLayout maploadLay;

    /**
     * The mview.
     */
    private MapView mview;

    private GoogleMap map;

    /**
     * The gp.
     */
    private LatLng gp;

    /** The mc. */
    //private MapController mc;

    /**
     * The Constant DLG_ID.
     */
    private static final int DLG_ID = 999;

    /**
     * The year.
     */
    private int year;

    /**
     * The month.
     */
    private int month;

    /**
     * The day.
     */
    private int day;

    /**
     * The isowner.
     */
    private String isowner;

    /**
     * The longi.
     */
    double longi;

    /**
     * The lat.
     */
    double lat;

    /**
     * The loct.
     */
    private Location loct;

    /**
     * The jparser2.
     */
    JSONParser jparser2 = new JSONParser();

    /**
     * The get_cause_data_url.
     */
    private String get_cause_data_url = "";

    /**
     * The Constant TAG_SUCCESS.
     */
    private static final String TAG_SUCCESS = "success";

    /**
     * The Constant TAG_MAIN_CAUSES.
     */
    private static final String TAG_MAIN_CAUSES = "MCauses";

    /**
     * The Constant TAG_VEHICLES.
     */
    private static final String TAG_VEHICLES = "Vehicle";

    /**
     * The Constant TAG_VEHICLE_NO.
     */
    private static final String TAG_VEHICLE_NO = "Vehicle";

    /**
     * The Constant TAG_SUB_CAUSES.
     */
    private static final String TAG_SUB_CAUSES = "SCauses";

    /**
     * The Constant TAG_MAIN_CAUSE.
     */
    private static final String TAG_MAIN_CAUSE = "MainCause";

    /**
     * The Constant TAG_SUB_CAUSE.
     */
    private static final String TAG_SUB_CAUSE = "SubCause";

    /**
     * The mcause.
     */
    private String mcause;

    /**
     * The scause.
     */
    private String scause;

    /**
     * The vehi_no.
     */
    private String vehi_no;

    /**
     * The g cusid.
     */
    private String gCusid;

    /**
     * The mcauses.
     */
    JSONArray mcauses = null;

    /**
     * The scauses.
     */
    JSONArray scauses = null;

    /**
     * The vehicles.
     */
    JSONArray vehicles = null;

    /**
     * The Mcauselist.
     */
    private ArrayList<String> Mcauselist;

    /**
     * The Scauselist.
     */
    private ArrayList<String> Scauselist;

    /**
     * The Vehiclelist.
     */
    private ArrayList<String> Vehiclelist;

    /*
     *
     *
     * @see com.google.android.maps.MapActivity#onCreate(android.os.Bundle)
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.create_incident);

        Bundle extras = getIntent().getExtras();
        gCusid = extras.getString("globalCusID");
        get_cause_data_url = getResources().getString(R.string.hostAddress)+"index_getcausedata.php";

        new SpinnerValues().execute();

        date = (EditText) findViewById(R.id.date_editText);
        setCurrentDate();
        date.setText(new StringBuilder().append(month + 1).append("/").append(day)
                .append("/").append(year).append(" "));

        date_pick = (Button) findViewById(R.id.datepick_button);
        date_pick.setOnClickListener(this);

        vehicle = (Spinner) findViewById(R.id.vehi_spinner);
        main_cause = (Spinner) findViewById(R.id.maincause_spinner);
        sub_cause = (Spinner) findViewById(R.id.subcause_spinner);
        owner = (CheckBox) findViewById(R.id.owner_checkBox);
        name = (EditText) findViewById(R.id.name_editText);
        licence_no = (EditText) findViewById(R.id.licenceno_editText);
        date = (EditText) findViewById(R.id.date_editText);
        get_curr_loc = (CheckBox) findViewById(R.id.getcurrloc_checkBox);
        location = (EditText) findViewById(R.id.loc_editText);
        speed = (EditText) findViewById(R.id.speed_editText);
        description = (EditText) findViewById(R.id.descrp_editText);
        map_load = (Button) findViewById(R.id.map_load_button);
        next = (Button) findViewById(R.id.next_ivw);

        next.setOnClickListener(this);
        map_load.setOnClickListener(this);
        get_curr_loc.setOnCheckedChangeListener(this);
    }

    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        switch (buttonView.getId()) {
            case R.id.getcurrloc_checkBox:
                if (isChecked == true) {
                    MyLocationActivity myloc = new MyLocationActivity();
                    myloc.getLoc();
                }
                break;

            default:
                break;
        }

    }

    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.next_ivw: {
                loadAddMoreDetails();

                break;
            }
            case R.id.map_load_button: {
                maploadLay = (LinearLayout) findViewById(R.id.mapLoading_layout);


                LayoutInflater layoutInf = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                View vw = layoutInf.inflate(R.layout.map, maploadLay, false);
                maploadLay.addView(vw);


                mview = (MapView) findViewById(R.id.mapView);

                map = mview.getMap();
                map.getUiSettings().setMyLocationButtonEnabled(true);
                map.setMyLocationEnabled(true);

                gp = new LatLng(lat, longi);

                //gp = new GeoPoint((int) (lat * 1E6), (int) (longi * 1E6));

                GroundOverlayOptions newarkMap = new GroundOverlayOptions()
                        .image(BitmapDescriptorFactory.fromResource(R.drawable.marker))
                        .position(gp, 8600f, 6500f);
                map.addGroundOverlay(newarkMap);

                break;
            }
            case R.id.datepick_button: {
                setCurrentDate();
                showDialog(DLG_ID);
            }
            default:
                break;
        }

    }

    /**
     * Load add more details.
     */
    public void loadAddMoreDetails() {

        if (owner.isChecked()) {
            isowner = "YES";
        } else {
            isowner = "NO";
        }

        Intent loadAddMoreInt = new Intent(this, AddMoreDetailsActivity.class);

        String values[] = {gCusid, vehicle.getSelectedItem().toString(),
                main_cause.getSelectedItem().toString(),
                sub_cause.getSelectedItem().toString(), isowner,
                name.getText().toString(), licence_no.getText().toString(),
                date.getText().toString(), location.getText().toString(),
                speed.getText().toString(), description.getText().toString(),
                "" + longi, "" + lat};

        loadAddMoreInt.putExtra("IncidentDataArray", values);
        loadAddMoreInt.putExtra("globalCusID", gCusid);

        startActivity(loadAddMoreInt);
    }

    /**
     * The Class MyLocationActivity.
     */
    public class MyLocationActivity implements LocationListener {

        /**
         * The is net provider ok.
         */
        boolean isNetProviderOk;

        /**
         * The is gps provider ok.
         */
        boolean isGPSProviderOk;

        /**
         * Gets the loc.
         *
         * @return the loc
         */
        public Location getLoc() {

            LocationManager loc_m = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

            isGPSProviderOk = loc_m
                    .isProviderEnabled(LocationManager.GPS_PROVIDER);
            isNetProviderOk = loc_m
                    .isProviderEnabled(LocationManager.NETWORK_PROVIDER);

            if (isGPSProviderOk) {
                loc_m.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0,
                        0, this);
                loct = loc_m.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                Log.v("GPS ", "GPS");
            }
            if (isNetProviderOk) {
                loc_m.requestLocationUpdates(LocationManager.NETWORK_PROVIDER,
                        0, 0, this);
                loct = loc_m
                        .getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                Log.v("Network ", "Network");
            }
            if (loct == null) {
                lat = 6.9167;
                longi = 79.8333;
            } else {
                lat = loct.getLatitude();
                longi = loct.getLongitude();
            }

            String loc_text = "Loc: Lat- " + lat + " Long- " + longi;

            location = (EditText) findViewById(R.id.loc_editText);

            location.setText(loc_text);

            return loct;
        }

        /*
         *
         *
         * @see
         * android.location.LocationListener#onLocationChanged(android.location
         * .Location)
         */
        public void onLocationChanged(Location location) {
            // TODO Auto-generated method stub
            lat = location.getLatitude();
            longi = location.getLongitude();

            //mc.animateTo(gp);
        }

        /*
         *
         *
         * @see
         * android.location.LocationListener#onProviderDisabled(java.lang.String
         * )
         */
        public void onProviderDisabled(String provider) {

        }

        /*
         *
         *
         * @see
         * android.location.LocationListener#onProviderEnabled(java.lang.String)
         */
        public void onProviderEnabled(String provider) {

        }

        /*
         *
         *
         * @see
         * android.location.LocationListener#onStatusChanged(java.lang.String,
         * int, android.os.Bundle)
         */
        public void onStatusChanged(String provider, int status, Bundle extras) {

        }

    }

    /**
     * The Class MyLocationOverlay.
     */

	/*protected class MyLocationOverlay extends com.google.android.maps.Overlay {

		*//*
         *
		 * 
		 * @see com.google.android.maps.Overlay#draw(android.graphics.Canvas,
		 * com.google.android.maps.MapView, boolean, long)
		 *//*
        @Override
		public boolean draw(Canvas canvas, MapView mapView, boolean shadow,
				long when) {
			Paint paint = new Paint();
			super.draw(canvas, mapView, shadow, when);
			Point myScreenCords = new Point();
			mapView.getProjection().toPixels(gp, myScreenCords);
			paint.setStrokeWidth(1);
			paint.setARGB(255, 255, 255, 255);
			paint.setStyle(Paint.Style.STROKE);
			Bitmap bmp = BitmapFactory.decodeResource(getResources(),
					R.drawable.marker);
			canvas.drawBitmap(bmp, myScreenCords.x, myScreenCords.y, paint);
			canvas.drawText("Current Location", myScreenCords.x,
					myScreenCords.y, paint);
			return true;

		}

		*//*
		 *  
		 * 
		 * @see
		 * com.google.android.maps.Overlay#onTouchEvent(android.view.MotionEvent
		 * , com.google.android.maps.MapView)
		 *//*
		@Override
		public boolean onTouchEvent(MotionEvent e, MapView mapView) {
			super.onTouchEvent(e, mapView);
			if (e.getAction() == MotionEvent.ACTION_UP) {
				GeoPoint gpoint = mview.getProjection().fromPixels(
						(int) e.getX(), (int) e.getY());

				location.setText("Loc: Lat- " + gpoint.getLatitudeE6()/1E6 + " Long- " + gpoint.getLongitudeE6()/1E6);
				mapView.getOverlays().remove(0);
				CenterLocation(gpoint);

			}

			return true;
		}

		private void CenterLocation(GeoPoint centerGeoPoint) {
			mc.animateTo(centerGeoPoint);

			placeMarker(centerGeoPoint.getLatitudeE6(),
					centerGeoPoint.getLongitudeE6());
		};

		private void placeMarker(int markerLatitude, int markerLongitude) {
			Drawable marker = getResources().getDrawable(R.drawable.marker);
			marker.setBounds(0, 0, marker.getIntrinsicWidth(),
					marker.getIntrinsicHeight());

			mview.getOverlays().add(new MyLocationOverlay());
		}

	}*/

	/*
	 *  
	 * 
	 * @see android.app.Activity#onCreateDialog(int)
	 */
    @Override
    protected Dialog onCreateDialog(int id) {
        switch (id) {
            case DLG_ID:
                return new DatePickerDialog(this, datePickerListner, year, month,
                        day);
        }
        return null;

    }

    /**
     * The date picker listner.
     */
    DatePickerDialog.OnDateSetListener datePickerListner = new OnDateSetListener() {

        public void onDateSet(DatePicker arg0, int y, int m, int d) {

            year = y;
            month = m;
            day = d;

            date.setText(new StringBuilder().append(month).append("/")
                    .append(day).append("/").append(year).append(" "));
        }
    };

    /*
     *
     *
     * @see android.app.Activity#onCreateOptionsMenu(android.view.Menu)
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.create_incident, menu);
        return true;
    }

    /**
     * Sets the current date.
     */
    void setCurrentDate() {
        final Calendar c = Calendar.getInstance();

        year = c.get(Calendar.YEAR);
        month = c.get(Calendar.MONTH);
        day = c.get(Calendar.DAY_OF_MONTH);

    }


    /**
     * The Class SpinnerValues.
     */
    class SpinnerValues extends AsyncTask<String, String, String> {

        /*
         *
         *
         * @see android.os.AsyncTask#doInBackground(Params[])
         */
        @Override
        protected String doInBackground(String... arg0) {

            Mcauselist = new ArrayList<String>();
            Scauselist = new ArrayList<String>();
            Vehiclelist = new ArrayList<String>();

            Uri.Builder builder = new Uri.Builder();

            JSONObject json = jparser2.makeHttpRequest(get_cause_data_url,
                    "GET", builder);
            Log.v("JSON Values: ", json.toString());

            try {
                // Checking for SUCCESS TAG
                int success = json.getInt(TAG_SUCCESS);

                if (success == 1) {
                    // products found
                    // Getting Array of Products
                    mcauses = json.getJSONArray(TAG_MAIN_CAUSES);
                    scauses = json.getJSONArray(TAG_SUB_CAUSES);
                    vehicles = json.getJSONArray(TAG_VEHICLES);

                    // looping through All Products
                    for (int i = 0; i < mcauses.length(); i++) {
                        JSONObject c = mcauses.getJSONObject(i);

                        // Storing each json item in variable
                        mcause = c.getString(TAG_MAIN_CAUSE);
                        // scause = c.getString(TAG_SUB_CAUSE);

                        // adding HashList to ArrayList
                        Mcauselist.add(mcause);
                        // causelist.add(scause);

                    }
                    for (int i = 0; i < scauses.length(); i++) {
                        JSONObject c = scauses.getJSONObject(i);

                        // Storing each json item in variable
                        scause = c.getString(TAG_SUB_CAUSE);
                        // scause = c.getString(TAG_SUB_CAUSE);

                        // adding HashList to ArrayList
                        Scauselist.add(scause);
                        // causelist.add(scause);

                    }
                    for (int i = 0; i < vehicles.length(); i++) {
                        JSONObject c = vehicles.getJSONObject(i);

                        // Storing each json item in variable
                        vehi_no = c.getString(TAG_VEHICLE_NO);
                        // scause = c.getString(TAG_SUB_CAUSE);

                        // adding HashList to ArrayList
                        Vehiclelist.add(vehi_no);
                        // causelist.add(scause);

                    }
                } else {
                    // no products found
                    // Launch Add New product Activity

                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        /*
         *
         *
         * @see android.os.AsyncTask#onPostExecute(java.lang.Object)
         */
        protected void onPostExecute(String file_url) {

            // updating UI from Background Thread
            runOnUiThread(new Runnable() {
                public void run() {
                    /**
                     * Updating parsed JSON data into Spinners
                     * */

                    main_cause = (Spinner) findViewById(R.id.maincause_spinner);
                    ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(
                            CreateIncidentActivity.this,
                            android.R.layout.simple_spinner_dropdown_item,
                            Mcauselist);
                    main_cause.setAdapter(spinnerArrayAdapter);

                    sub_cause = (Spinner) findViewById(R.id.subcause_spinner);
                    ArrayAdapter<String> spinnerArrayAdapter2 = new ArrayAdapter<String>(
                            CreateIncidentActivity.this,
                            android.R.layout.simple_spinner_dropdown_item,
                            Scauselist);
                    sub_cause.setAdapter(spinnerArrayAdapter2);

                    vehicle = (Spinner) findViewById(R.id.vehi_spinner);
                    ArrayAdapter<String> spinnerArrayAdapter3 = new ArrayAdapter<String>(
                            CreateIncidentActivity.this,
                            android.R.layout.simple_spinner_dropdown_item,
                            Vehiclelist);
                    vehicle.setAdapter(spinnerArrayAdapter3);

                }

            });

        }

    }

}
