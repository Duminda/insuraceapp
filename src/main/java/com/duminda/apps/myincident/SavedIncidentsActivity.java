package com.duminda.apps.myincident;

import android.app.ListActivity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;

import com.duminda.apps.createincidents.IncidentsData;
import com.duminda.apps.common.DBHelper;
import com.duminda.apps.insuranceapp.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

// TODO: Auto-generated Javadoc

/**
 * The Class SavedIncidentsActivity.
 */
public class SavedIncidentsActivity extends ListActivity {

    /**
     * The saved incidents.
     */
    private ArrayList<HashMap<String, String>> savedIncidents;

    /**
     * The Constant TAG_VEHI_NO.
     */
    private static final String TAG_VEHI_NO = "vehino";

    /**
     * The Constant TAG_TIME.
     */
    private static final String TAG_TIME = "time";

    /**
     * The Constant TAG_STATUS.
     */
    private static final String TAG_STATUS = "status";

    /**
     * The Constant TAG_LOC.
     */
    private static final String TAG_LOC = "location";

    /**
     * The Constant TAG_SPEED.
     */
    private static final String TAG_SPEED = "speed";

    /**
     * The Constant TAG_DESCR.
     */
    private static final String TAG_DESCR = "descripction";

    /**
     * The type.
     */
    private static String TYPE;

    /**
     * The Constant TAG_CUSID.
     */
    private static final String TAG_CUSID = "CusID";

    /**
     * The Constant TAG_MAIN_CLAUSE.
     */
    private static final String TAG_MAIN_CLAUSE = "MainCause";

    /**
     * The Constant TAG_SUB_CLAUSE.
     */
    private static final String TAG_SUB_CLAUSE = "SubCause";

    /**
     * The Constant TAG_OWNER.
     */
    private static final String TAG_OWNER = "Owner";

    /**
     * The Constant TAG_NAME.
     */
    private static final String TAG_NAME = "Name";

    /**
     * The Constant TAG_LICENCE_NO.
     */
    private static final String TAG_LICENCE_NO = "LicenceNo";

    /**
     * The Constant TAG_LAT.
     */
    private static final String TAG_LAT = "Latitude";

    /**
     * The Constant TAG_LON.
     */
    private static final String TAG_LON = "Longitude";

    /* (non-Javadoc)
     * @see android.app.Activity#onCreate(android.os.Bundle)
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.saved_incidents);

        savedIncidents = new ArrayList<HashMap<String, String>>();
        new LoadSavedIncidents().execute();

        ListView lv = getListView();

        lv.setOnItemClickListener(new OnItemClickListener() {

            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                HashMap<String, String> arr = savedIncidents.get(position);

                String[] arr2 = {arr.get(TAG_TIME), arr.get(TAG_LOC),
                        arr.get(TAG_SPEED), arr.get(TAG_DESCR),
                        arr.get(TAG_CUSID), arr.get(TAG_MAIN_CLAUSE),
                        arr.get(TAG_SUB_CLAUSE), arr.get(TAG_OWNER),
                        arr.get(TAG_NAME), arr.get(TAG_LICENCE_NO),
                        arr.get(TAG_VEHI_NO), arr.get(TAG_LAT),
                        arr.get(TAG_LON)};

                launchIncidentSummary(arr2);

            }

        });

    }

    /* (non-Javadoc)
     * @see android.app.Activity#onCreateOptionsMenu(android.view.Menu)
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.saved_incidents, menu);
        return true;
    }

    /**
     * Launch incident summary.
     *
     * @param arr the arr
     */
    public void launchIncidentSummary(String[] arr) {

        Intent incSummInt = new Intent(this, MyIncidentSummaryActivity.class);
        incSummInt.putExtra("SavedInci", arr);
        startActivity(incSummInt);
    }

    /**
     * The Class LoadSavedIncidents.
     */
    class LoadSavedIncidents extends AsyncTask<String, String, String> {

        /* (non-Javadoc)
         * @see android.os.AsyncTask#doInBackground(Params[])
         */
        @Override
        protected String doInBackground(String... arg0) {

            DBHelper db = new DBHelper(getApplicationContext());
            List<IncidentsData> savedInc = db.getAllIncedents();

            HashMap<String, String> map = new HashMap<String, String>();

            for (IncidentsData idata : savedInc) {

                map.put(TAG_VEHI_NO, idata.getVahicle());
                map.put(TAG_TIME, idata.getDt());
                map.put(TAG_STATUS, "saved");
                map.put(TAG_LOC, idata.getLocation());
                map.put(TAG_DESCR, idata.getDesription());
                map.put(TAG_SPEED, idata.getSpeed());
                map.put(TAG_CUSID, idata.getCusId());
                map.put(TAG_MAIN_CLAUSE, idata.getMclause());
                map.put(TAG_SUB_CLAUSE, idata.getSclause());
                map.put(TAG_OWNER, idata.getOwner());
                map.put(TAG_NAME, idata.getNm());
                map.put(TAG_LICENCE_NO, idata.getLicenNo());
                map.put(TAG_LAT, idata.getLatitude());
                map.put(TAG_LON, idata.getLongitude());

                map.put(TYPE, idata.getMclause() + " - " + idata.getSclause());

                savedIncidents.add(map);

            }

            return null;
        }

        /* (non-Javadoc)
         * @see android.os.AsyncTask#onPostExecute(java.lang.Object)
         */
        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            ListAdapter adapter = new SimpleAdapter(
                    SavedIncidentsActivity.this, savedIncidents,
                    R.layout.incident_list_item, new String[]{TAG_VEHI_NO,
                    TAG_TIME, TYPE, TAG_STATUS}, new int[]{
                    R.id.vehi_no_txtview, R.id.date_txtview,
                    R.id.type_txtview, R.id.status_txtview});

            setListAdapter(adapter);

        }
    }
}
