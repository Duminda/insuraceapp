package com.duminda.apps.myincident;

import android.app.ListActivity;
import android.app.ProgressDialog;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Menu;
import android.widget.ListAdapter;
import android.widget.SimpleAdapter;

import com.duminda.apps.common.JSONParser;
import com.duminda.apps.insuranceapp.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

// TODO: Auto-generated Javadoc

/**
 * The Class PendingIncidentsActivity.
 */
public class PendingIncidentsActivity extends ListActivity {

    /**
     * The pdialog.
     */
    private ProgressDialog pdialog;

    /**
     * The jparser.
     */
    JSONParser jparser = new JSONParser();

    /**
     * The policylist.
     */
    private ArrayList<HashMap<String, String>> incidentlist;

    /**
     * The policy_records_url.
     */
    private String pending_incidents_url = "";

    /**
     * The Constant TAG_P_INCI.
     */
    private static final String TAG_P_INCI = "Incidents";

    /**
     * The Constant TAG_SUCCESS.
     */
    private static final String TAG_SUCCESS = "success";

    /**
     * The Constant TAG_POLICIES.
     */
    private static final String TAG_VEHI_ID = "Vehicle";

    /**
     * The Constant TAG_POL_ID.
     */
    private static final String TAG_DATE = "Date";

    /**
     * The Constant TAG_CUS_ID.
     */
    private static final String TAG_MAINCAUSE = "MainClause";

    /**
     * The Constant TAG_DAYS_REM.
     */
    private static final String TAG_STATUS = "Status";

    /**
     * The vehi_no.
     */
    private String vehi_no;

    /**
     * The ext_giv.
     */
    private String date;

    /**
     * The ext_app.
     */
    private String main_cause;

    /**
     * The g cusid.
     */
    private String status = "Pending";

    /**
     * The p_incidents.
     */
    JSONArray p_incidents = null;

    /* (non-Javadoc)
     * @see android.app.Activity#onCreate(android.os.Bundle)
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pending_incidents);

        pending_incidents_url = getResources().getString(R.string.hostAddress)+"index_getIncidents.php";

        incidentlist = new ArrayList<HashMap<String, String>>();

        new LoadPendingIncidents().execute();
    }

    /*
     * (non-Javadoc)
     *
     * @see android.app.Activity#onCreateOptionsMenu(android.view.Menu)
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.pending_incidents, menu);
        return true;
    }

    /**
     * The Class LoadAllPolicies.
     */
    class LoadPendingIncidents extends AsyncTask<String, String, String> {

        /*
         * (non-Javadoc)
         *
         * @see android.os.AsyncTask#onPreExecute()
         */
        @Override
        protected void onPreExecute() {

            super.onPreExecute();

            pdialog = new ProgressDialog(PendingIncidentsActivity.this);
            pdialog.setMessage("Loading Incidents ....");
            pdialog.setIndeterminate(false);
            pdialog.setCancelable(false);
            pdialog.show();
        }

        @Override
        protected String doInBackground(String... arg0) {


            try {

                Uri.Builder builder = new Uri.Builder();
                builder.appendQueryParameter(TAG_STATUS, status);


                JSONObject json = jparser.makeHttpRequest(
                        pending_incidents_url, "GET", builder);

                //Log.v("JSON policy data : ", json.toString());

                int success = json.getInt(TAG_SUCCESS);

                if (success == 1) {

                    p_incidents = json.getJSONArray(TAG_P_INCI);

                    for (int i = 0; i < p_incidents.length(); i++) {
                        JSONObject c = p_incidents.getJSONObject(i);

                        vehi_no = c.getString(TAG_VEHI_ID);
                        date = c.getString(TAG_DATE);
                        main_cause = c.getString(TAG_MAINCAUSE);
                        status = c.getString(TAG_STATUS);

                        HashMap<String, String> map = new HashMap<String, String>();

                        map.put(TAG_VEHI_ID, vehi_no);
                        map.put(TAG_DATE, date);
                        map.put(TAG_MAINCAUSE, main_cause);
                        map.put(TAG_STATUS, status);

                        incidentlist.add(map);

                    }
                } else {

                }
            } catch (JSONException e) {
                e.printStackTrace();
            }


            return null;
        }

        /*
         * (non-Javadoc)
         *
         * @see android.os.AsyncTask#onPostExecute(java.lang.Object)
         */
        protected void onPostExecute(String file_url) {
            // dismiss the dialog after getting all products
            pdialog.dismiss();

            // updating UI from Background Thread
            runOnUiThread(new Runnable() {
                public void run() {
                    /**
                     * Updating parsed JSON data into ListView
                     * */
                    ListAdapter adapter = new SimpleAdapter(
                            PendingIncidentsActivity.this, incidentlist,
                            R.layout.incident_list_item, new String[]{
                            TAG_VEHI_ID, TAG_DATE, TAG_MAINCAUSE,
                            TAG_STATUS}, new int[]{
                            R.id.vehi_no_txtview, R.id.date_txtview,
                            R.id.type_txtview, R.id.status_txtview});
                    // updating listview
                    setListAdapter(adapter);

                }

            });

        }

    }
}
