package com.duminda.apps.myincident;

import android.app.TabActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.widget.TabHost;
import android.widget.TabHost.TabSpec;

import com.duminda.apps.insuranceapp.R;

// TODO: Auto-generated Javadoc

/**
 * The Class MyIncidentListActivity.
 */
public class MyIncidentListActivity extends TabActivity {

    /* (non-Javadoc)
     * @see android.app.ActivityGroup#onCreate(android.os.Bundle)
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.my_incident_list);

        TabHost tabh = getTabHost();

        TabSpec savedInc = tabh.newTabSpec("Saved");
        savedInc.setIndicator("Save", getResources().getDrawable(R.drawable.tabhoststyle));
        Intent savedInt = new Intent(this, SavedIncidentsActivity.class);
        savedInc.setContent(savedInt);

        TabSpec pendngInc = tabh.newTabSpec("Pending");
        pendngInc.setIndicator("Pending", getResources().getDrawable(R.drawable.tabhoststyle));
        Intent pendInt = new Intent(this, PendingIncidentsActivity.class);
        pendngInc.setContent(pendInt);

        TabSpec apprvdInc = tabh.newTabSpec("Approved");
        apprvdInc.setIndicator("Approved", getResources().getDrawable(R.drawable.tabhoststyle));
        Intent apprvInt = new Intent(this, ApprovedIncidentsActivity.class);
        apprvdInc.setContent(apprvInt);

        TabSpec rejctdInc = tabh.newTabSpec("Rejected");
        rejctdInc.setIndicator("Rejected", getResources().getDrawable(R.drawable.tabhoststyle));
        Intent rejctInt = new Intent(this, RejectedIncidentsActivity.class);
        rejctdInc.setContent(rejctInt);

        tabh.addTab(savedInc);
        tabh.addTab(pendngInc);
        tabh.addTab(apprvdInc);
        tabh.addTab(rejctdInc);
    }

    /* (non-Javadoc)
     * @see android.app.Activity#onCreateOptionsMenu(android.view.Menu)
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.my_incident_summary, menu);
        return true;
    }
}
