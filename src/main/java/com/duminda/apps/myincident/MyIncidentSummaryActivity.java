package com.duminda.apps.myincident;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.duminda.apps.common.DBHelper;
import com.duminda.apps.common.JSONParser;
import com.duminda.apps.insuranceapp.MainActivity;
import com.duminda.apps.insuranceapp.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

// TODO: Auto-generated Javadoc

/**
 * The Class MyIncidentSummaryActivity.
 */
public class MyIncidentSummaryActivity extends Activity implements
        View.OnClickListener {

    /**
     * The time.
     */
    private EditText time;

    /**
     * The location.
     */
    private EditText location;

    /**
     * The latitude.
     */
    private EditText latitude;

    /**
     * The longitude.
     */
    private EditText longitude;

    /**
     * The speed.
     */
    private EditText speed;

    /**
     * The description.
     */
    private EditText description;

    /**
     * The tag.
     */
    private TextView tag;

    /**
     * The notes.
     */
    private TextView notes;

    /**
     * The vehiimage.
     */
    private ImageView vehiimage;

    /**
     * The delete.
     */
    private Button delete;

    /**
     * The edit.
     */
    private Button edit;

    /**
     * The array saved inc data.
     */
    private String arraySavedIncData[];

    /**
     * The Constant TAG_SUCCESS.
     */
    private static final String TAG_SUCCESS = "success";

    /**
     * The Constant TAG_VEHI_CAT.
     */
    private static final String TAG_VEHI_CAT = "GetImage";

    /**
     * The Constant TAG_VEHI_TYPE.
     */
    private static final String TAG_VEHI_TYPE = "Type";

    /**
     * The Constant TAG_VEHI_NOTES.
     */
    private static final String TAG_VEHI_NOTES = "Notes";

    /**
     * The Constant TAG_VEHI_IMAGE_TITLE.
     */
    private static final String TAG_VEHI_IMAGE_TITLE = "Title";

    /**
     * The vtype.
     */
    private String vtype;

    /**
     * The vnotes.
     */
    private String vnotes;

    /**
     * The vtitle.
     */
    private String vtitle;

    /**
     * The json parser.
     */
    JSONParser jsonParser = new JSONParser();

    /**
     * The images.
     */
    JSONArray images = null;

    /**
     * The Constant image_get_url.
     */
    private String image_get_url = "";

    /* (non-Javadoc)
     * @see android.app.Activity#onCreate(android.os.Bundle)
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.my_incident_summary);

        image_get_url = getResources().getString(R.string.hostAddress)+"index_imageget.php";

        Bundle extras = getIntent().getExtras();
        arraySavedIncData = extras.getStringArray("SavedInci");

        time = (EditText) findViewById(R.id.inc_sum_date_txt);
        location = (EditText) findViewById(R.id.inc_sum_loc_txt);
        latitude = (EditText) findViewById(R.id.inc_sum_lat_txt);
        longitude = (EditText) findViewById(R.id.inc_sum_long_txt);
        speed = (EditText) findViewById(R.id.inc_sum_speed_txt);
        description = (EditText) findViewById(R.id.inc_sum_descr_txt);
        vehiimage = (ImageView) findViewById(R.id.load_my_vehi_img);
        tag = (TextView) findViewById(R.id.load_vehi_type);
        notes = (TextView) findViewById(R.id.load_notes);

        time.setText(arraySavedIncData[0]);
        time.setEnabled(false);
        location.setText(arraySavedIncData[1]);
        location.setEnabled(false);
        speed.setText(arraySavedIncData[2]);
        speed.setEnabled(false);
        description.setText(arraySavedIncData[3]);
        description.setEnabled(false);
        latitude.setText(arraySavedIncData[11]);
        latitude.setEnabled(false);
        longitude.setText(arraySavedIncData[12]);
        longitude.setEnabled(false);

        //new GetImage().execute();

        delete = (Button) findViewById(R.id.inc_sum_del_ivw);
        edit = (Button) findViewById(R.id.inc_sum_edit_ivw);

        delete.setOnClickListener(this);

        edit.setOnClickListener(this);

    }

    /**
     * Delete incident.
     */
    void deleteIncident() {

        DBHelper dbhelper = new DBHelper(getApplicationContext());

        dbhelper.deleteLatestContact();

        Intent backtoHomeInt = new Intent(this, MainActivity.class);
        startActivity(backtoHomeInt);
    }

    /**
     * Edits the incident.
     */
    void editIncident() {

        Intent summaryEditInt = new Intent(this,
                IncidentSummaryEditActivity.class);
        summaryEditInt.putExtra("editableData", arraySavedIncData);
        startActivity(summaryEditInt);
    }

    /* (non-Javadoc)
     * @see android.app.Activity#onCreateOptionsMenu(android.view.Menu)
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.my_incident_summary, menu);
        return true;
    }

    /*
     * (non-Javadoc)
     *
     * @see android.view.View.OnTouchListener#onTouch(android.view.View,
     * android.view.MotionEvent)
     */
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.inc_sum_del_ivw: {
                deleteIncident();
            }
            break;

            case R.id.inc_sum_edit_ivw: {
                editIncident();
            }
            break;

        }
    }

    /**
     * The Class GetImage.
     */
    class GetImage extends AsyncTask<String, String, String> {

        /* (non-Javadoc)
         * @see android.os.AsyncTask#onPreExecute()
         */
        @Override
        protected void onPreExecute() {

            super.onPreExecute();
        }

        /* (non-Javadoc)
         * @see android.os.AsyncTask#doInBackground(Params[])
         */
        @Override
        protected String doInBackground(String... arg0) {

            Uri.Builder builder = new Uri.Builder();

            JSONObject json = jsonParser.makeHttpRequest(image_get_url, "GET",
                    builder);
            //Log.d("Image data", json.toString());

            try {
                int success = json.getInt(TAG_SUCCESS);

                if (success == 1) {

                    images = json.getJSONArray(TAG_VEHI_CAT);

                    for (int i = 0; i < images.length(); i++) {
                        JSONObject c = images.getJSONObject(i);

                        vtype = c.getString(TAG_VEHI_TYPE);
                        vnotes = c.getString(TAG_VEHI_NOTES);
                        vtitle = c.getString(TAG_VEHI_IMAGE_TITLE);
                    }

                } else {
                    // failed to update product
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }

        /* (non-Javadoc)
         * @see android.os.AsyncTask#onPostExecute(java.lang.Object)
         */
        @Override
        protected void onPostExecute(String result) {

            tag.setText(vtype);
            notes.setText(vnotes);

            try {

                String image_url = "http://iapp.net78.net/" + vtitle;

                URL thumb_u = new URL(image_url);
                Drawable thumb_d = Drawable.createFromStream(
                        thumb_u.openStream(), "src");

                Bitmap d = ((BitmapDrawable) thumb_d).getBitmap();
                Bitmap bitmapOrig = Bitmap.createScaledBitmap(d, 200, 200,
                        false);
                BitmapDrawable dout = new BitmapDrawable(bitmapOrig);

                vehiimage.setImageDrawable(dout);

            } catch (MalformedURLException e) {

                e.printStackTrace();
            } catch (IOException e) {

                e.printStackTrace();
            }

            super.onPostExecute(result);
        }

    }
}
