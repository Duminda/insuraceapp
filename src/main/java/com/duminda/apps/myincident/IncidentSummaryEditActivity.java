package com.duminda.apps.myincident;

import android.app.Activity;
import android.app.ProgressDialog;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.duminda.apps.common.JSONParser;
import com.duminda.apps.insuranceapp.R;

import org.json.JSONException;
import org.json.JSONObject;

// TODO: Auto-generated Javadoc

/**
 * The Class IncidentSummaryEditActivity.
 */
public class IncidentSummaryEditActivity extends Activity implements
        View.OnClickListener {

    /**
     * The time.
     */
    private EditText time;

    /**
     * The location.
     */
    private EditText location;

    /**
     * The latitude.
     */
    private EditText latitude;

    /**
     * The longitude.
     */
    private EditText longitude;

    /**
     * The speed.
     */
    private EditText speed;

    /**
     * The description.
     */
    private EditText description;

    /**
     * The submit.
     */
    private Button submit;

    /**
     * The p dialog.
     */
    private ProgressDialog pDialog;

    /**
     * The array saved inc data.
     */
    private String arraySavedIncData[];

    /**
     * The jparser3.
     */
    JSONParser jparser3 = new JSONParser();

    /**
     * The create_incident_url.
     */
    private String update_incident_url = "";

    /**
     * The Constant TAG_SUCCESS.
     */
    private static final String TAG_SUCCESS = "success";

    /**
     * The Constant TAG_CUSID.
     */
    private static final String TAG_CUSID = "CusID";

    /**
     * The Constant TAG_VEHI_NO.
     */
    private static final String TAG_VEHI_NO = "Vehicle";

    /**
     * The Constant TAG_MAIN_CLAUSE.
     */
    private static final String TAG_MAIN_CLAUSE = "MainCause";

    /**
     * The Constant TAG_SUB_CLAUSE.
     */
    private static final String TAG_SUB_CLAUSE = "SubCause";

    /**
     * The Constant TAG_OWNER.
     */
    private static final String TAG_OWNER = "Owner";

    /**
     * The Constant TAG_NAME.
     */
    private static final String TAG_NAME = "Name";

    /**
     * The Constant TAG_LICENCE_NO.
     */
    private static final String TAG_LICENCE_NO = "LicenceNo";

    /**
     * The Constant TAG_TIME.
     */
    private static final String TAG_TIME = "Date";

    /**
     * The Constant TAG_LOC.
     */
    private static final String TAG_LOC = "Location";

    /**
     * The Constant TAG_SPEED.
     */
    private static final String TAG_SPEED = "Speed";

    /**
     * The Constant TAG_DESCR.
     */
    private static final String TAG_DESCR = "Description";

    /*
     * @see android.app.Activity#onCreate(android.os.Bundle)
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.incident_summary_edit);

        update_incident_url = getResources().getString(R.string.hostAddress)+"index_updateIncident.php";

        Bundle extras = getIntent().getExtras();
        arraySavedIncData = extras.getStringArray("editableData");

        time = (EditText) findViewById(R.id.inc_sum_date_txt_edit);
        location = (EditText) findViewById(R.id.inc_sum_loc_txt_edit);
        latitude = (EditText) findViewById(R.id.inc_sum_lat_txt_edit);
        longitude = (EditText) findViewById(R.id.inc_sum_long_txt_edit);
        speed = (EditText) findViewById(R.id.inc_sum_speed_txt_edit);
        description = (EditText) findViewById(R.id.inc_sum_descr_txt_edit);

        submit = (Button) findViewById(R.id.submit_ivw);

        time.setText(arraySavedIncData[0]);
        location.setText(arraySavedIncData[1]);
        speed.setText(arraySavedIncData[2]);
        description.setText(arraySavedIncData[3]);
        latitude.setText(arraySavedIncData[11]);
        longitude.setText(arraySavedIncData[12]);

        submit.setOnClickListener(this);

    }

    /*
     * @see android.view.View.OnTouchListener#onTouch(android.view.View, android.view.MotionEvent)
     */
    public void onClick(View v) {

        arraySavedIncData[0] = time.getText().toString();
        arraySavedIncData[1] = location.getText().toString();
        arraySavedIncData[2] = speed.getText().toString();
        arraySavedIncData[3] = description.getText().toString();
        arraySavedIncData[11] = latitude.getText().toString();
        arraySavedIncData[12] = longitude.getText().toString();

        new SubmitEditedIncident().execute();
    }

    /*
     * @see android.app.Activity#onCreateOptionsMenu(android.view.Menu)
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.incident_summary_edit, menu);
        return true;
    }

    /**
     * The Class SubmitEditedIncident.
     */
    class SubmitEditedIncident extends AsyncTask<String, String, String> {

        /*
         * @see android.os.AsyncTask#onPreExecute()
         */
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(IncidentSummaryEditActivity.this);
            pDialog.setMessage("Saving product ...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }

        /*
         * @see android.os.AsyncTask#doInBackground(Params[])
         */
        @Override
        protected String doInBackground(String... arg0) {
            runOnUiThread(new Runnable() {
                public void run() {

                    try {

                        Uri.Builder builder = new Uri.Builder();

                        builder.appendQueryParameter(TAG_TIME, arraySavedIncData[0]);
                        builder.appendQueryParameter(TAG_LOC, arraySavedIncData[1]);
                        builder.appendQueryParameter(TAG_SPEED, arraySavedIncData[2]);
                        builder.appendQueryParameter(TAG_DESCR, arraySavedIncData[3]);
                        builder.appendQueryParameter(TAG_CUSID, arraySavedIncData[4]);
                        builder.appendQueryParameter(TAG_MAIN_CLAUSE, arraySavedIncData[5]);
                        builder.appendQueryParameter(TAG_SUB_CLAUSE, arraySavedIncData[6]);
                        builder.appendQueryParameter(TAG_OWNER, arraySavedIncData[7]);
                        builder.appendQueryParameter(TAG_NAME, arraySavedIncData[8]);
                        builder.appendQueryParameter(TAG_LICENCE_NO, arraySavedIncData[9]);
                        builder.appendQueryParameter(TAG_VEHI_NO, arraySavedIncData[10]);


                        JSONObject json = jparser3.makeHttpRequest(
                                update_incident_url, "POST", builder);

                        //Log.v("JSON incident data : ", json.toString());

                        // Checking for SUCCESS TAG
                        int success = json.getInt(TAG_SUCCESS);

                        if (success == 1) {
                            Toast.makeText(getApplicationContext(),
                                    "Incident Data Saved Successfully !",
                                    Toast.LENGTH_SHORT).show();

                        } else {
                            Toast.makeText(getApplicationContext(),
                                    "Incident Data Save Failed !",
                                    Toast.LENGTH_SHORT).show();

                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });

            return null;
        }

        /*
         * @see android.os.AsyncTask#onPostExecute(java.lang.Object)
         */
        @Override
        protected void onPostExecute(String result) {

            super.onPostExecute(result);
            pDialog.dismiss();
        }

    }

}
